<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('redsysAPI');
        date_default_timezone_set('America/Asuncion');
    }

    public function index() {
        $this->loadView('main');
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    function pages($titulo) {
        $titulo = urldecode(str_replace("-", "+", $titulo));
        if (!empty($titulo)) {
            $pagina = $this->db->get_where('paginas', array('titulo' => $titulo));
            if ($pagina->num_rows() > 0) {
                $this->loadView(array('view' => 'paginas', 'contenido' => $pagina->row()->contenido, 'title' => $titulo));
            } else
                $this->loadView('404');
        }
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $this->load->view('template', $param);
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }

    function sendMailer() {
        $this->load->library('mailer');
        $this->mailer->mail('joncar.c@gmail.com', 'Test', 'Test');
    }
    
    function procesar_pago(){           
        if (!empty($_POST)){            
            $this->db->update('pagos_clientes',array('status'=>2),array('nro_pago'=>$_POST['Num_operacion']));
            $this->db->select('user.*');
            $this->db->join('user','user.id = pagos_clientes.user_id');
            $cliente = $this->db->get_where('pagos_clientes',array('nro_pago'=>$_POST['Num_operacion']));
            $var = '<h1>Pago Recibido por '.$cliente->row()->nombre.'</h1>';
            $var.= '<h2>Datos del cliente</h2>';
            foreach($cliente->row() as $n=>$v){
                 $var.= '<p><b>'.$n.'</b>: '.$v.'</p>';
            }
            $var.= '<h2>Datos del pago</h2>';
            foreach($_POST as $n=>$v){
                $var.= '<p><b>'.$n.'</b>: '.$v.'</p>';
            }
            //Envio al vendedor
            correo('joncar.c@gmail.com','Pago Recibido',$var);
            correo('info@hipo.tv','Pago Recibido',$var);
            correo('reservas@miex.me','Pago Recibido',$var);
            //Envio al comprador
            correo($cliente->row()->email,'Gracias por su pago','Hola '.$cliente->row()->nombre.' Gracias por tu pago, cada vez estás más cerca de tu gran viaje. Si tienes cualquier consulta no dudes en ponerte en contacto con info@miex.me.');
        }
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
            //correo('joncar.c@gmail.com',$mensaje->titulo,$mensaje->texto);
            correo('info@hipo.tv',$mensaje->titulo,$mensaje->texto);
        }
    
    function mostrarlic(){
        $this->load->view('licencia');
    }
    
    function unsubscribe(){
        if(empty($_POST)){
            $this->loadView('includes/template/unsubscribe');
        }else{
            $emails = $this->db->get_where('invitaciones',array('email'=>$_POST['email']));
            $success = $emails->num_rows()>0?TRUE:FALSE;
            if($success){
                $this->db->delete('invitaciones',array('email'=>$_POST['email']));                
            }            
            $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
        }
    }

}
