<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Boletines extends Panel{
        function __construct() {
            parent::__construct();

        }
        function boletin(){
            $this->norequireds = array('emails');
            $crud = $this->crud_function('','');
            $emails = array();
            foreach($this->db->get('invitaciones')->result() as $e){
                $emails[] = $e->email;
            }
            
            $crud->set_field_upload('banner','img/boletines');
            $crud->unset_columns('contenido');
            $crud->field_type('emails','set',$emails);
            $crud->add_action('<i class="fa fa-envelope"></i> Añadir Cuerpo','',base_url('boletines/boletin_detalles').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Ver Boletín','',base_url('boletines/ver').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar Boletín','',base_url('boletines/send').'/');
            $action = $crud->getParameters();            
            $crud = $crud->render();
                        
            $this->loadView($crud);
        }                   
        
        function boletin_detalles($x = ''){
            $crud = $this->crud_function('','');
            if(is_numeric($x)){
                $crud->field_type('boletin_id','hidden',$x);
                $crud->unset_columns('boletin_id','texto');
            }
            $crud->set_field_upload('foto','images/boletines');
            $crud = $crud->render();                        
            $this->loadView($crud);
        }                
        
        function ver($id = ''){
            if(is_numeric($id)){
                $detail = $this->db->get_where('boletin',array('id'=>$id))->row();
                $detail->cuerpo = $this->db->get_where('boletin_detalles',array('boletin_id'=>$detail->id));
                $view = empty($detail->contenido)?'boletin':'boletinr';                
                $this->load->view('frontend/'.$view,array('detail'=>$detail));                
            }                        
        }
        
        function send($id = '',$emails = ''){
            if(is_numeric($id)){
                $this->db->query("SET NAMES 'utf8'");
                $detail = $this->db->get_where('boletin',array('id'=>$id))->row();
                $detail->cuerpo = $this->db->get_where('boletin_detalles',array('boletin_id'=>$detail->id));
                $view = empty($detail->contenido)?'boletin':'boletinr';
                $correo = $this->load->view('frontend/'.$view,array('detail'=>$detail),TRUE);
                $correo = $correo;
                //echo $correo;       
                ob_end_flush();
                ob_start();
                set_time_limit(0);
                if(empty($detail->emails) && empty($emails)){
                    foreach($this->db->get_where('invitaciones')->result() as $e){
                        $mail = str_replace('{nro}',$e->nro,$correo);
                        correo($e->email,$detail->titulo,  $mail);
                        echo "Enviado a ".$e->email.'<br/>';
                        ob_flush();
                        flush();
                    }
                }else{
                    $detail->emails = !empty($emails)?$emails:$detail->emails;
                    foreach(explode(',',$detail->emails) as $e){
                        //Verificar si tiene nro
                        $em = $this->db->get_where('invitaciones',array('email'=>$e));
                        if($em->num_rows()>0){
                            $mail = str_replace('{nro}',$em->row()->nro,$correo);
                            correo($e,$detail->titulo,$mail);
                        }
                    }
                }
                echo "Correo enviado a todos los contactos registrados en la tabla emails <a href='javascript:history.back()'>Volver a la lista</a>";
            }                        
        }
        
        function sendFromId($x = ''){
            if(is_numeric($x)){
                $x = $this->db->get_where('invitaciones',array('id'=>$x));
                if($x->num_rows()>0){
                    if($x->row()->status==-1){
                        $this->db->update('invitaciones',array('status'=>0),array('id'=>$x->row()->id));
                    }
                    $this->send(1,$x->row()->email);
                }
            }
        }
        
        
        function getLastInput(){
            $this->db->order_by('id','DESC');
            $this->db->limit('1');
            $last = $this->db->get_where('boletin');
            if($last->num_rows()>0){
                echo $last->row()->texto;
            }
        }
    }
?>
