<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="ISO-8859-1">
        <meta name="Viewport" content="width=device-width, initial-scale=1.0">
        <style type="text/css" id="Mail Designer General Style Sheet">
            a { word-break: break-word; }
            a img { border:none; }
            img { outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }
            body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family:'Arial' !important;}
            .ExternalClass { width: 100%; }
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
            #page-wrap { margin: 0; padding: 0; width: 100% !important;}
            #outlook a { padding: 0; }
            .preheader { display:none !important; }
            a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: 'Arial' !important; font-weight: inherit !important; line-height: inherit !important; }
        </style>
        <style type="text/css" id="Mail Designer Mobile Style Sheet">
            @media only screen and (max-width: 580px) {
                table[class=email-body-wrap] {
                    width: 320px !important;
                }
                td[class=page-bg-show-thru] {
                    display: none !important;
                }
                table[class=layout-block-wrapping-table] {
                    width: 320px !important;
                }
                table[class=mso-fixed-width-wrapping-table] {
                    width: 320px !important;
                }
                *[class=layout-block-full-width] {
                    width: 320px !important;
                }
                table[class=layout-block-column], table[class=layout-block-padded-column] {
                    width: 100% !important;
                }
                table[class=layout-block-box-padding] {
                    width: 100% !important;
                    padding: 5px !important;
                }
                table[class=layout-block-horizontal-spacer] {
                    display: none !important;
                }
                tr[class=layout-block-vertical-spacer] {
                    display: block !important;
                    height: 8px !important;
                }
                td[class=container-padding] {
                    display: none !important;
                }

                table {
                    min-width: initial !important;
                }
                td {
                    min-width: initial !important;
                }

                *[class~=desktop-only] { display: none !important; }
                *[class~=mobile-only] { display: block !important; }

                .hide {
                    max-height: none !important;
                    display: block !important;
                    overflow: visible !important;
                }

                div[eqID="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-7FC5D3D0-8C6D-4FEE-9CF1-3545D1D19E3E"] { height:17px !important; } /* vertical spacer */
            }
        </style>
        <!--[if gte mso 9]>
        <style type="text/css" id="Mail Designer Outlook Style Sheet">
                table.layout-block-horizontal-spacer {
                    display: none !important;
                }
                table {
                    border-collapse:collapse;
                    mso-table-lspace:0pt;
                    mso-table-rspace:0pt;
                    mso-table-bspace:0pt;
                    mso-table-tspace:0pt;
                    mso-padding-alt:0;
                    mso-table-top:0;
                    mso-table-wrap:around;
                }
                td {
                    border-collapse:collapse;
                    mso-cellspacing:0;
                }
        </style>
        <![endif]-->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:700,regular" rel="stylesheet" type="text/css" class="EQWebFont">
        
    </head>
    <body style="margin-top: 0px; margin-right: 0px; margin-bottom: 50px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 50px; padding-left: 0px; " eqid="EQMST-5B629705-2068-4643-A1B4-1BA752D88F04" background="<?= base_url('img/boletines')?>/page-bg.jpg">
        <center><a href="<?= base_url('boletines/frontend/ver/'.$detail->id) ?>" style="color:#333">Si no veus bé aquest email fes clik aquí</a></center>
        <!--[if gte mso 9]>
        <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" src="<?= base_url('img/boletines')?>/page-bg.jpg" />
        </v:background>
        <![endif]-->
        <table id="page-wrap" width="100%" align="center" cellspacing="0" cellpadding="0" background="<?= base_url('img/boletines')?>/page-bg.jpg">
            <tbody>
                <tr>
                    <td>
                        <table class="email-body-wrap" id="email-body" width="726" align="center" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="page-bg-show-thru" width="30">&nbsp;<!--Left page bg show-thru --></td>
                                    <td id="page-body" eqid="EQMST-E42EE1D2-FD64-4CC3-81A8-CEDDEEA78958" width="666" bgcolor="#ffffff" background="<?= base_url('img/boletines')?>/background.jpg">

                                        <!--Begin of layout container -->
                                        <div id="eqLayoutContainer" style="height: auto; ">
                                            
                                            
                                            <div eqid="EQMST-E7DD055F-9768-41B1-B11F-7203C5F91402" width="100%" class="desktop-only">
                                                <table class="mso-fixed-width-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td style="min-width: 666px; " width="666">
                                                                                <div class="layout-block-image">
                                                                                        <!--[if gte mso 9]>
                                                                                            <img width="666" height="131" alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889"src="<?= base_url('img/boletines')?>/image-3.png" border="0" style="display: block; width: 666px; height: 131px;"><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]--><a href="http://www.bstim.cat"><!--[if gte mso 9]><img width="666" height="131" alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889" src="<?= base_url('img/boletines')?>/image-3.png" border="0" style="display: block; width: 666px; height: 131px;"><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]--><img alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889" src="<?= base_url('img/boletines')?>/image-3.png" style="width: 666px; height: 131px; display: block; " width="666" height="131" border="0"><!--[if gte mso 9]></div><![endif]--></a>
                                                                                    <!--[if gte mso 9]></div><![endif]-->
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%" eqid="EQMST-5FCF1FB0-06C7-4DBF-9C38-98D3D32C7BBE" class="mobile-only hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; ">
                                                <table class="mso-fixed-width-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 320px; " width="320" valign="top">
                                                                <table class="layout-block-full-width hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td style="min-width: 320px; " width="320">
                                                                                <div class="layout-block-image hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; ">
                                                                                    <img alt="" eqid="EQMST-C85E404B-222F-4BD1-B410-7D945C51E731" src="<?= base_url('img/boletines')?>/image-5.jpg" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; width: 320px; height: 144px; " class="hide" width="320" height="144" border="0">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-188CA4C8-0993-41A1-96D4-3F44AF105B12" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="min-width: 666px; " width="666">
                                                                                <div class="layout-block-image">
                                                                                        <!--[if gte mso 9]><a href="<?= $detail->link_banner ?>"><img width="666" height="394" alt="" eqid="EQMST-A3DFEC46-51FA-451A-ACDD-D6ABFF7CCE7F" src="<?= base_url('img/boletines/'.$detail->banner)?>" border="0" style="display: block; width: 666px; height: 394px;"></a><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]--><a href='<?= $detail->link_banner ?>'><img alt="" eqid="EQMST-A3DFEC46-51FA-451A-ACDD-D6ABFF7CCE7F" src="<?= base_url('img/boletines/'.$detail->banner)?>" style="width: 666px; height: 394px; display: block; " width="666" height="394" border="0"></a>
                                                                                    <!--[if gte mso 9]></div><![endif]--></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%" eqid="EQMST-F3370785-473B-4F8E-82BC-57FADBF50FD7" class="mobile-only hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; ">
                                                <table class="layout-block-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 320px; " width="320" valign="top">
                                                                <table class="layout-block-full-width hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td style="min-width: 320px; " width="320">
                                                                                <div class="layout-block-image hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; ">
                                                                                    <a href='<?= $detail->link_banner ?>'><img alt="" eqid="EQMST-AEB42D1F-6CB6-471E-ADD2-9BF45C7DF26C" src="<?= base_url('img/boletines/'.$detail->banner)?>" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; width: 320px; height: 172px; " class="hide" width="320" height="172" border="0"></a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php foreach($detail->cuerpo->result() as $c): ?>
                                            <!--------- Comienza contenido ---------->
                                            <div eqid="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D" width="100%" style="margin-top:15px; margin-bottom:15px;">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-23860111-7423-4AD5-9AB8-FB4626A266B0" class="desktop-only" width="100%">
                                                <table class="mso-fixed-width-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-content-cell" eqid="EQMST-A92510C3-ACA8-41FD-AEAB-8686AB13634B" style="min-width: 666px; " width="666" valign="top" align="left" bgcolor="#ffffff" background="<?= base_url('img/boletines')?>/box-bg-1.jpg">
                                                                <table class="mso-fixed-width layout-block-content-cell" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="layout-block-content-cell" style="min-width: 666px; " width="666" align="left"><!--[if mso]><table width="666" border="0" cellpadding="0" cellspacing="0" align="center"><tr><![endif]-->
                                                                                    <!--[if mso]><td width="241"><![endif]-->
                                                                                <table class="layout-block-full-width" style="min-width: 241px; " width="241" align="left" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="layout-block-column" style="min-width: 241px; " width="241" valign="top" align="left">
                                                                                                <div class="layout-block-image">
                                                                                                        <!--[if gte mso 9]>
                                                                                                        <img width="241" alt="" eqid="EQMST-0EBC43D5-3A59-4C1A-B28F-C1C606F72CFD" src="<?= base_url('img/boletines/'.$c->foto)?>" border="0" eqoriginalbackground="<?= base_url('img/boletines')?>/image-6.png" style="display: block; width: 241px; height: auto">
                                                                                                        <div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]-->
                                                                                                    <img alt="" eqid="EQMST-0EBC43D5-3A59-4C1A-B28F-C1C606F72CFD" src="<?= base_url('img/boletines/'.$c->foto)?>" style="width: 241px; display: block;" width="241" border="0">
                                                                                                    <!--[if gte mso 9]></div><![endif]-->
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                    <!--[if mso]></td><td width="425"><![endif]-->
                                                                                    <table class="layout-block-full-width" style="min-width: 425px; " width="425" align="left" cellspacing="0" cellpadding="0">
                                                                                    <!--[if !mso]><!-->
                                                                                    <tbody>
                                                                                        
                                                                                        <!--<![endif]-->
                                                                                        <tr>
                                                                                            <td class="layout-block-padding-left" style="min-width: 33px; " width="33">&nbsp;</td>
                                                                                            <td class="layout-block-content-cell" style="min-width: 359px; " width="359" valign="top" align="left">
                                                                                                <div class="text" eqid="EQMST-57A7DC5C-6959-48D7-8BF0-AF913A625979" style="font-size: 16px; font-family: 'Lucida Grande', 'Arial'; ">
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 20px; font-weight: bold; line-height: 1.2; text-align: left; ">
                                                                                                        <span style="color: rgb(0, 0, 0); "><?= $c->titulo_ingles ?></span>
                                                                                                    </div>
                                                                                                    <a href="<?= base_url('main/traduccion/en').'?url='.$c->url_ingles ?>" style="color: rgb(126, 221, 56); font-family: 'Open Sans', 'Arial', 'Helvetica Neue', 'Arial', sans-serif; font-size: 12px; font-style: normal; font-weight: 400; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                        <font color="#ae1916">Read More</font>
                                                                                                    </a>
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 20px; font-weight: bold; line-height: 1.2; text-align: left; ">
                                                                                                        <span style="color: rgb(0, 0, 0); "><?= $c->titulo_catalan ?></span>
                                                                                                    </div>
                                                                                                    <a href="<?= base_url('main/traduccion/ca').'?url='.$c->url_catalan ?>" style="color: rgb(126, 221, 56); font-family: 'Open Sans', 'Arial', 'Helvetica Neue', 'Arial', sans-serif; font-size: 12px; font-style: normal; font-weight: 400; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                        <font color="#ae1916">Llegir més</font>
                                                                                                    </a>
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 20px; font-weight: bold; line-height: 1.2; text-align: left; ">
                                                                                                        <span style="color: rgb(0, 0, 0); "><?= $c->titulo ?></span>
                                                                                                    </div>
                                                                                                    <a href="<?= base_url('main/traduccion/es').'?url='.$c->url ?>" style="color: rgb(126, 221, 56); font-family: 'Open Sans', 'Arial', 'Helvetica Neue', 'Arial', sans-serif; font-size: 12px; font-style: normal; font-weight: 400; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                        <font color="#ae1916">Leer más</font>
                                                                                                    </a>
                                                                                                    <div style="color: rgb(0, 0, 0); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 32px; font-style: normal; font-weight: normal; line-height: 1.5; text-align: left; text-decoration: none; ">
                                                                                                    </div>                                                                                                        
                                                                                                </div>
                                                                                            </td>
                                                                                            <td class="layout-block-padding-right" style="min-width: 33px; " width="33">&nbsp;</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!--[if mso]></tr></table><![endif]-->
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; " eqid="EQMST-7A56E6FB-89E8-4CE7-BF4C-5CBF509C2073" class="mobile-only hide" width="100%">
                                                <table class="mso-fixed-width-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-content-cell" style="min-width: 320px; " eqid="EQMST-976F560B-D832-4209-92A1-424E272C9D8C" width="320" valign="top" align="left" bgcolor="#ffffff" background="<?= base_url('img/boletines')?>/box-bg-2.jpg">
                                                                <table class="mso-fixed-width layout-block-content-cell hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td class="layout-block-content-cell" style="min-width: 320px; " width="320" align="left">
                                                                                <table class="layout-block-full-width hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" align="left" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="layout-block-column" style="min-width: 320px; " width="320" valign="top" align="left">
                                                                                                <div class="layout-block-image hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; ">
                                                                                                    <img alt="" eqid="EQMST-8ACCD72C-9B1B-4910-940F-145382858D95" src="<?= base_url('img/boletines/'.$c->foto)?>" class="hide" style="display: none; width: 320px; height: auto; max-height: 0px; overflow: hidden; mso-hide: all; " width="320" border="0">
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table class="layout-block-full-width hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" align="left" cellspacing="0" cellpadding="0">

                                                                                    <tbody>
                                                                                        <tr class="layout-block-vertical-spacer" style="display:none; height:0px; ">
                                                                                            <td style="min-width: 25px; " width="25"><div class="spacer"></div></td>
                                                                                            <td style="min-width: 270px; " width="270"></td>
                                                                                            <td style="min-width: 25px; " width="25"></td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td class="layout-block-padding-left" style="min-width: 25px; " width="25">&nbsp;</td>
                                                                                            <td class="layout-block-content-cell" style="min-width: 270px; " width="270" valign="top" align="left">
                                                                                                <div class="text" eqid="EQMST-CBDD2E93-5706-4E3B-A9CE-C73F1F60B650" style="font-size: 16px; font-family: 'Lucida Grande', 'Arial'; ">
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 20px; font-weight: bold; line-height: 1.2; text-align: left; ">
                                                                                                        <span style="color: rgb(0, 0, 0); "><?= $c->titulo_ingles ?></span>
                                                                                                    </div>
                                                                                                    <a href="<?= base_url('main/traduccion/en').'?url='.$c->url_ingles ?>" style="color: rgb(126, 221, 56); font-family: 'Open Sans', 'Arial', 'Helvetica Neue', 'Arial', sans-serif; font-size: 12px; font-style: normal; font-weight: 400; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                        <font color="#ae1916">Read More</font>
                                                                                                    </a>
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 20px; font-weight: bold; line-height: 1.2; text-align: left; ">
                                                                                                        <span style="color: rgb(0, 0, 0); "><?= $c->titulo_catalan ?></span>
                                                                                                    </div>
                                                                                                    <a href="<?= base_url('main/traduccion/ca').'?url='.$c->url_catalan ?>" style="color: rgb(126, 221, 56); font-family: 'Open Sans', 'Arial', 'Helvetica Neue', 'Arial', sans-serif; font-size: 12px; font-style: normal; font-weight: 400; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                        <font color="#ae1916">Llegir més</font>
                                                                                                    </a>
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 20px; font-weight: bold; line-height: 1.2; text-align: left; ">
                                                                                                        <span style="color: rgb(0, 0, 0); "><?= $c->titulo ?></span>
                                                                                                    </div>
                                                                                                    <a href="<?= base_url('main/traduccion/es').'?url='.$c->url ?>" style="color: rgb(126, 221, 56); font-family: 'Open Sans', 'Arial', 'Helvetica Neue', 'Arial', sans-serif; font-size: 12px; font-style: normal; font-weight: 400; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                        <font color="#ae1916">Leer mas</font>
                                                                                                    </a>
                                                                                                    <div style="color: rgb(0, 0, 0); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 32px; font-style: normal; font-weight: normal; line-height: 1.5; text-align: left; text-decoration: none; ">
                                                                                                    </div></div>
                                                                                            </td>
                                                                                            <td class="layout-block-padding-right" style="min-width: 25px; " width="25">&nbsp;</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-A8B3B39F-10E0-40EA-B9F9-9E132D0DFF9C" class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                            <td class="layout-block-padding-left" style="min-width: 33px; " width="33">&nbsp;</td>
                                                            <td class="layout-block-content-cell" eqid="EQMST-25FE1786-EE7C-4027-A95E-C87018BCF30B" style="min-width: 600px; " width="600" bgcolor="#ffffff" background="<?= base_url('img/boletines')?>/box-bg-3.jpg">
                                                                <table style="padding-left: 10px; padding-right: 10px; min-width: 600px; " class="layout-block-box-padding" width="600" align="left" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="layout-block-padded-column" style="min-width: 580px; " width="580" valign="top" align="center">
                                                                                <div class="layout-block-image">
                                                                                        <!--[if gte mso 9]><img width="580" height="1" alt="" eqid="EQMST-5F31893F-EC2A-40F3-9019-15C7DF074171" src="<?= base_url('img/boletines')?>/image-9.png" border="0" eqoriginalbackground="<?= base_url('img/boletines')?>/image-8.png" style="display: block; width: 580px; height: 1px;"><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]-->
                                                                                    <img alt="" eqid="EQMST-5F31893F-EC2A-40F3-9019-15C7DF074171" src="<?= base_url('img/boletines')?>/image-9.png" style="width: 580px; height: 1px; display: block; " width="580" height="1" border="0">
                                                                                    <!--[if gte mso 9]></div><![endif]-->
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="layout-block-padding-right" style="min-width: 33px; " width="33">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; " eqid="EQMST-72AFE327-5CED-4C92-BC23-3DC7A50128E4" class="mobile-only hide" width="100%" background="<?= base_url('img/boletines')?>/background-1.jpg">
                                                <table class="layout-block-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0" bgcolor="#fbfffa" background="<?= base_url('img/boletines')?>/background-1.jpg">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-padding-left" style="min-width: 19px; " width="19">&nbsp;</td>
                                                            <td class="layout-block-content-cell" style="min-width: 282px; " eqid="EQMST-144D0DDB-02E3-4EAF-B076-25593B46DFEC" width="282" bgcolor="#ffffff" background="<?= base_url('img/boletines')?>/box-bg-10.jpg">
                                                                <table style="padding-left: 10px; padding-right: 10px; display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 282px; " class="layout-block-box-padding hide" width="282" align="left" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td class="layout-block-padded-column" style="min-width: 262px; " width="262" valign="top" align="center">
                                                                                <div class="layout-block-image hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; ">
                                                                                    <img alt="" eqid="EQMST-3C37932A-1F2E-48F8-ABBB-F17302683119" src="<?= base_url('img/boletines')?>/image-13.png" class="hide" style="display: none; width: 262px; height: 23px; max-height: 0px; overflow: hidden; mso-hide: all; " width="262" height="23" border="0">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="layout-block-padding-right" style="min-width: 19px; " width="19">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php endforeach ?>
                                            <!---------- FIN CONTENIDO ------------>
                                            <div eqid="EQMST-66A68E4D-7B41-487F-833C-BCCA15FB7D50" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="min-width: 260px;text-align:center" width="260">
                                                                                
                                                                              </td>
                                                                              <td>
                                                                                <a href="<?= site_url() ?>"><img src="<?= base_url('img/boletines/logo.png' ) ?>"style="margin-top: 30px " ></a>
                                                                              </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            

                                            <div width="100%" eqid="EQMST-1FC5CE77-C861-41F2-881E-4E7A975FF07A" class="mobile-only hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; margin-top: 7px;">
                                                <table class="layout-block-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                            <td class="layout-block-full-width" style="min-width: 320px; " width="320" valign="top">
                                                                <table class="layout-block-full-width hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="text-align: center; min-width: 230px;" width="130">
                                                                                
                                                                              </td>
                                                                              <td>
                                                                                <a href="<?= site_url() ?>" style="text-decoration:none">
                                                                                    <img src="http://bstim.cat/blog/img/boletines/logo.png" style=" width: 80px; margin-top: 10px; margin-left: -10px; margin-bottom: 20px ">
                                                                                </a>
                                                                                <br/>
                                                                                <a href="https://www.facebook.com/bstimfair/" style="text-decoration:none">
                                                                                    <img src="http://bstim.cat/blog/img/boletines/face.png" style="">
                                                                                </a>
                                                                                <a href="https://twitter.com/bstim_fair" style="text-decoration:none">
                                                                                    <img src="http://bstim.cat/blog/img/boletines/twit.png" style="padding-bottom: 3px;">
                                                                                </a>
                                                                                
                                                                              </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>
                                            </div>

                                            <div eqid="EQMST-22C8A407-2599-4B34-98E8-A70458F2ACA3" class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-padding-left" style="min-width: 25px; " width="25">&nbsp;</td>
                                                            <td class="layout-block-content-cell" style="min-width: 616px; " width="616" valign="top" align="left">
                                                                <table class="layout-block-column" style="min-width: 616px; " width="616" align="left" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-left: 10px; padding-right: 10px; min-width: 596px; " eqid="EQMST-6F1F647C-304E-4260-A64E-BFC53CA5C677" width="596" valign="top" align="left" bgcolor="#ffffff" background="<?= base_url('img/boletines')?>/box-bg-2.jpg">
                                                                                <table class="layout-block-box-padding" style="min-width: 596px; " width="596" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="layout-block-column" style="min-width: 596px; " width="596" align="left">
                                                                                                <div class="heading" eqid="EQMST-3B228E9E-2621-4001-908E-74A5025C2D05" style="font-size: 16px; font-family: 'Lucida Grande', 'Arial'; text-align: center; ">
                                                                                                    <a href="https://www.facebook.com/bstimfair/"><img src="<?= base_url('img/boletines/face.png') ?>" style="float:left;"></a>
                                                                                                    <a href="https://twitter.com/bstim_fair"><img src="<?= base_url('img/boletines/twit.png') ?>" style="float:left; padding-top: 8px;"></a>
                                                                                
                                                                                                    <a href="http://bstim.cat/avis-legal.html" style="color: rgb(94, 94, 94); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 13px; margin-left: -70px;">
                                                                                                        Avis Legal  
                                                                                                    </a>| 
                                                                                                    <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 13px; ">    
                                                                                                        Donar-se de baixa
                                                                                                    </a>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="layout-block-padding-right" style="min-width: 25px; " width="25">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; " eqid="EQMST-FCDD6FA3-CF8E-41CC-9CFD-75501AE32C20" class="mobile-only hide" background="<?= base_url('img/boletines')?>/background-2.jpg">
                                                <table class="layout-block-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0" bgcolor="#ffffff" background="<?= base_url('img/boletines')?>/background-2.jpg">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-padding-left" style="min-width: 15px; " width="15">&nbsp;</td>
                                                            <td class="layout-block-content-cell" style="min-width: 290px; " width="290" valign="top" align="left">
                                                                <table class="layout-block-column hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 290px; " width="290" align="left" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-left: 10px; padding-right: 10px; min-width: 270px; " eqid="EQMST-1AE683C0-6D4C-4055-820A-174A10512B82" width="270" valign="top" align="left" bgcolor="#ffffff" background="<?= base_url('img/boletines')?>/box-bg-10.jpg">
                                                                                <table class="layout-block-box-padding hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 270px; " width="270" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="layout-block-column" style="min-width: 270px; " width="270" align="left">
                                                                                                <div class="heading" style="font-size: 16px; font-family: 'Lucida Grande', 'Arial'; text-align: center;  margin-top: 60px " eqid="EQMST-05B94A82-9C8E-469C-B219-ED3EB3450257">
                                                                                                    <a href="http://bstim.cat/avis-legal.html" style="color: rgb(94, 94, 94); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 13px; ">
                                                                                                        Avis Legal  
                                                                                                    </a>|
                                                                                                    <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 13px; ">    
                                                                                                        Donar-se de baixa
                                                                                                    </a>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="layout-block-padding-right" style="min-width: 15px; " width="15">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-22C8A407-2599-4B34-98E8-A70458F2ACA3" style="background-image:url(background-3.jpg); background-position: 0% 0%; " class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0" bgcolor="#fffeff" background="<?= base_url('img/boletines')?>/background-3.jpg">
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; " eqid="EQMST-6A6B0687-E4D6-4886-B45A-43BA011F8747" class="mobile-only hide" width="100%" background="<?= base_url('img/boletines')?>/background-4.jpg">
                                                <table class="layout-block-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden; mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0" bgcolor="#fffeff" background="<?= base_url('img/boletines')?>/background-4.jpg">
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        <!--End of layout container -->

                                    </td>
                                    <td class="page-bg-show-thru" width="30">&nbsp;<!--Right page bg show-thru --></td>
                                </tr>
                            </tbody>
                        </table><!--email-body -->
                    </td>
                </tr>
            </tbody>
        </table><!--page-wrap -->
    </body>
</html>