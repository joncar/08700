<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="Generator" content="Made with Mail Designer from equinux">
        <meta name="Viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans:700,regular" rel="stylesheet" type="text/css" class="EQWebFont">
        <link href="https://fonts.googleapis.com/css?family=Roboto:regular" rel="stylesheet" type="text/css" class="EQWebFont">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:regular,700" rel="stylesheet" type="text/css" class="EQWebFont">	
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; " background="<?= base_url() ?>img/boletines/page-bg.jpg">
        <table width="100%" cellspacing="0" cellpadding="0" id="page-wrap" align="center" background="<?= base_url() ?>img/boletines/page-bg.jpg">
            <tbody>
                <tr>
                    <td>
                        <table class="email-body-wrap" width="662" cellspacing="0" cellpadding="0" id="email-body" align="center">
                            <tbody>
                                <tr>
                                    <td width="30" class="page-bg-show-thru">&nbsp;<!--Left page bg show-thru --></td>
                                    <td width="602" id="page-body" bgcolor="">

                                        <!--Begin of layout container -->
                                        <div id="eqLayoutContainer">

                                            <div style="background-image:url(<?= base_url() ?>img/boletines/background-3.jpg); color: rgb(255, 255, 255); font-family: 'Butler Stencil', Arial, 'Butler Stencil', 'Droid Sans', sans-serif; font-size: 43px; font-style: normal; font-weight: 700; line-height: 0.5; margin-bottom: 0px; margin-top: 0px; text-align: left; text-decoration: none; background-position: 0% 0%; " width="100%">
                                                <table width="602" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" background="<?= base_url() ?>img/boletines/background-3.jpg" bgcolor="#292b2c" style="min-width: 602px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="22" style="min-width: 22px; ">&nbsp;</td>
                                                            <td valign="top" align="left" width="558" style="min-width: 558px; ">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="layout-block-column" width="558" style="min-width: 558px; ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="558" valign="top" align="left" style="min-width: 558px; ">
                                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding" width="558" style="min-width: 558px; ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="layout-block-column" width="558" style="min-width: 558px; ">
                                                                                                <div class="heading" style="font-size: 12px; font-family: 'Open Sans', Arial, Arial, sans-serif; color: rgb(108, 108, 108); font-style: normal; font-weight: 400; line-height: 34px; margin-bottom: 0pt; margin-top: 0pt; text-align: center; text-decoration: none; ">
                                                                                                    <font color="#7f7f7f">Si no podeu veure correctament aquest missatge premeu</font> <a href="http://08700.cat/boletines/frontend/ver/1" style="color: rgb(0, 144, 123); ">AQUÍ</a>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="22" style="min-width: 22px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%">
                                                <table width="602" cellspacing="0" cellpadding="0" class="mso-fixed-width-wrapping-table" style="min-width: 602px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" class="layout-block-full-width" width="602" style="min-width: 602px; ">
                                                                <table cellspacing="0" cellpadding="0" class="layout-block-full-width" width="602" style="min-width: 602px; ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="602" style="min-width: 602px; ">
                                                                                <div class="layout-block-image">
                                                                                    <a href="http://www.08700.cat">
                                                                                        <img width="602" height="502" alt="" src="<?= base_url() ?>img/boletines/image-1.jpg" border="0" style="line-height: 1px; display: block; width: 602px; height: 502px; ">
                                                                                    </a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%">
                                                <table width="602" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" style="min-width: 602px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="602" style="background:url('<?= base_url() ?>img/boletines/box-bg-4.jpg'); background-image:url('<?= base_url() ?>img/boletines/box-bg-4.jpg');" background="<?= base_url() ?>img/boletines/box-bg-4.jpg" bgcolor="#38393b">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="mso-fixed-width" width="602" style="min-width: 602px; ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="293" valign="top" align="left" style="min-width: 293px; ">
                                                                                <table width="293" cellspacing="0" cellpadding="0" align="left" class="layout-block-full-width" style="min-width: 293px; ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" valign="top" class="layout-block-column" width="293" style="min-width: 293px; ">
                                                                                                <div class="layout-block-image">
                                                                                                    <img width="293" height="584" alt="" src="<?= base_url() ?>img/boletines/image-3.jpg" border="0" style="line-height: 1px; display: block; width: 293px; height: 584px; ">
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                            <td width="309" valign="top" align="left" style="min-width: 309px; ">
                                                                                <table width="309" cellspacing="0" cellpadding="0" align="left" class="layout-block-full-width" style="min-width: 309px; ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td width="23" style="min-width: 23px; ">&nbsp;</td>
                                                                                            <td align="center" width="263" style="min-width: 263px; ">
                                                                                                <div class="layout-block-image">
                                                                                                    <img width="263" height="258" alt="" src="<?= base_url() ?>img/boletines/image-4.png" border="0" style="line-height: 1px; display: block; width: 263px; height: 258px; ">
                                                                                                </div>
                                                                                            </td>
                                                                                            <td width="23" style="min-width: 23px; ">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr class="layout-block-vertical-spacer" style="display:block; height:8px; ">
                                                                                            <td width="23">
                                                                                                <div class="spacer"></div>
                                                                                            </td>
                                                                                            <td width="263">
                                                                                                <div class="spacer"></div>
                                                                                            </td>
                                                                                            <td width="23">
                                                                                                <div class="spacer"></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="23" style="min-width: 23px; ">&nbsp;</td>
                                                                                            <td align="left" valign="top" width="263" style="min-width: 263px; ">
                                                                                                <div class="text" style="font-size: 16px; font-family: 'Lucida Grande'; ">
                                                                                                    <p style="margin: 0px; font-size: 22px; line-height: normal; font-family: 'Open Sans'; color: rgb(255, 255, 255); ">
                                                                                                        <span style="font-kerning: none; ">{nro}</p>
                                                                                                    <div>
                                                                                                        <span style="font-kerning: none; "><br></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td width="23" style="min-width: 23px; ">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr class="layout-block-vertical-spacer" style="display:block; height:8px; ">
                                                                                            <td width="23">
                                                                                                <div class="spacer"></div>
                                                                                            </td>
                                                                                            <td width="263">
                                                                                                <div class="spacer"></div>
                                                                                            </td>
                                                                                            <td width="23">
                                                                                                <div class="spacer"></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="23" style="min-width: 23px; ">&nbsp;</td>
                                                                                            <td align="center" width="263" style="min-width: 263px; ">
                                                                                                <div class="layout-block-image">
                                                                                                    <a href="http://www.08700.cat">
                                                                                                        <img width="263" height="252" alt="" src="<?= base_url() ?>img/boletines/image-5.png" border="0" style="line-height: 1px; display: block; width: 263px; height: 252px; ">
                                                                                                    </a>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td width="23" style="min-width: 23px; ">&nbsp;</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>                                                                                    
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>                                                                    
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div width="100%">
                                                <table width="602" cellspacing="0" cellpadding="0" class="mso-fixed-width-wrapping-table" style="min-width: 602px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" class="layout-block-full-width" width="602" style="min-width: 602px; ">
                                                                <table cellspacing="0" cellpadding="0" class="layout-block-full-width" width="602" style="min-width: 602px; ">
                                                                    <tbody><tr>
                                                                            <td width="602" style="min-width: 602px; ">
                                                                                <div class="layout-block-image">
                                                                                    <a href="http://www.08700.cat">
                                                                                        <img width="602" height="138" alt="" src="<?= base_url() ?>img/boletines/image-7.jpg" border="0" style="display: block; width: 602px; height: 138px; ">
                                                                                    </a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div style="background-image:url(<?= base_url() ?>img/boletines/background-3.jpg); background-position: 0% 0%; " width="100%">
                                                <table width="602" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" background="<?= base_url() ?>img/boletines/background-3.jpg" bgcolor="#292b2c" style="min-width: 602px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                            <td width="580" style="min-width: 580px; ">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="layout-block-column" width="580" style="min-width: 580px; ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="560" valign="top" align="left" style="padding-left: 10px; padding-right: 10px; min-width: 560px; ">
                                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding" width="560" style="min-width: 560px; ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="layout-block-column" width="560" style="min-width: 560px; ">
                                                                                                <div class="text" style="text-align: center; font-size: 16px; font-family: 'Lucida Grande'; "><div style="text-align: center; "><span style="color: rgb(94, 94, 94); font-family: Arial, 'Helvetica Neue'; font-size: 11px; "><br></span></div><div style="text-align: center; "><span style="color: rgb(94, 94, 94); font-family: Arial, 'Helvetica Neue'; font-size: 11px; "><br></span></div><div><br></div><div style="color: rgb(255, 255, 255); font-family: 'Open Sans', Arial, 'Butler Stencil', 'Droid Sans', sans-serif; font-size: 11px; font-style: normal; font-weight: normal; line-height: 1.2; margin-bottom: 0px; margin-top: 0px; text-align: justify; text-decoration: none; ">En compliment de la LOPD 15/99 i LSSI 34/2002, i altres disposicions legals, l'informem que les seves dades de caràcter personal passaran a formar part d'un fitxer automatitzat de caràcter personal el Responsable del qual és FAGEPI. Autoritza la utilització dels mateixos per a les comunicacions, incloent les realitzades via correu electrònic, que FAGEPI realitzi amb finalitats promocionals o informatives de les activitats que organitza i/o dóna suport amb la seva logística. En cas que contracti algun servei amb FAGEPI, queda informat que les seves dades podran ser comunicades, amb obligació de confidencialitat, a les empreses col·laboradores, sempre que això sigui necessari a les fins que aquestes realitzin el servei contractat. Queda igualment informat de la possibilitat d'exercitar sobre les dades els drets d'accés, rectificació, cancel·lació i oposició, per la qual cosa haurà de dirigir-se per e-mail <a href="mailto:angels@fagepi.net" style="text-align: center; color: rgb(89, 167, 157); ">
                                                                                                            <b>angels@fagepi.net</b>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                        <br>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                        <br>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div style="background-image:url(<?= base_url() ?>img/boletines/background-3.jpg); background-position: 0% 0%; " width="100%">
                                                <table width="602" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" background="<?= base_url() ?>img/boletines/background-3.jpg" bgcolor="#292b2c" style="min-width: 602px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                            <td width="580" style="min-width: 580px; ">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="layout-block-column" width="580" style="min-width: 580px; ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="560" valign="top" align="left" style="padding-left: 10px; padding-right: 10px; min-width: 560px; ">
                                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding" width="560" style="min-width: 560px; ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="layout-block-column" width="560" style="min-width: 560px; ">
                                                                                                <div class="text" style="text-align: center; font-size: 16px; font-family: 'Lucida Grande'; ">
                                                                                                    <a href="mailto: angels@fagepi.net" style="color: rgb(191, 191, 191); font-size: 11px; ">Contacte</a>&nbsp;
                                                                                                    <span style="color: rgb(94, 94, 94); font-family: Arial, 'Helvetica Neue'; font-size: 11px; ">| </span>
                                                                                                    <a href="http://08700.cat/main/unsubscribe" style="font-family: Arial, 'Helvetica Neue'; font-size: 11px; color: rgb(191, 191, 191); ">Donar-se de baixa</a>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div style="background-image:url(<?= base_url() ?>img/boletines/background-3.jpg); background-position: 0% 0%; " width="100%">
                                                <table width="602" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" background="<?= base_url() ?>img/boletines/background-3.jpg" bgcolor="#292b2c" style="min-width: 602px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="11" style="font-size: 1px; min-width: 11px; ">&nbsp;</td>
                                                            <td height="20" width="580" style="min-width: 580px; ">
                                                                <div class="spacer"></div>
                                                            </td>
                                                            <td width="11" style="font-size: 1px; min-width: 11px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                            
                                    </td>
                                    <td width="30" class="page-bg-show-thru">&nbsp;<!--Right page bg show-thru --></td>
                                </tr>
                        </table><!--email-body -->
                    </td>
                </tr>
            </tbody>
        </table><!--page-wrap -->
    </body>
</html>