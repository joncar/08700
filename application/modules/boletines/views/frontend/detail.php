<div class="about-page loaded" id="page-content">
<header data-bg="<?= base_url('images/hoteles/'.$detail->portada) ?>" class="overlay" style="background-image: url(<?= base_url() ?>images/about_header_bg2.jpg);">
    <?= $this->load->view('includes/template/menu2') ?>
    <div class="header-center-content"> 
        <div class="container text-center"> 
            <div class="row"> 
                <div class="col-md-offset-2 col-md-8 animated fadeInUp"> 
                 <i class="icon icon-dial"style=" font-size: 60px; color: #f71259;"></i>
                    <h1 class="text-uppercase"><?= $detail->nombre ?></h1> 
                    <h4>Empieza a conocer todo lo que incluimos</h4> 
                </div> 
            </div> 
        </div> 
    </div> 
</header> <!-- /.about page header --> <!-- main content --> 
<main> 
    
    <section id="trip-experience"> 
        <div class="container"> <!-- section-intro --> 
            <div class="row text-center section-intro bordered">
                <div class="col-md-12"> 
               
                    </p>
                </div> 
            </div> <!-- /.section-intro --> <!-- small intro --> 
            <div class="row"> 
                <div class="col-md-offset-2 col-md-8">
                    <div class="small-intro">
                        <?= $detail->descripcion ?>
                    </div>
                </div> 
            </div> <!-- /.small intro --> <!-- trip background -->                 
        </div> 
    </section>
    
    <section id="trip-actividades-extras" style="margin:85px 0;"> <!-- trip activities background --> 
        <div data-bg="<?= base_url() ?>images/trip-activities-bg.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/trip-activities-bg.jpg); visibility: visible; animation-name: slideInLeft;">

        </div> <!-- /.trip activities background --> 
        <div class="container"> 
            <div class="row"> <!-- trip activities --> 
                <div class="col-md-offset-4 col-md-9 col-sm-offset-6 col-sm-6"> 
                    <div class="section-intro"> 
                        <i data-wow-delay="0.4s" class="icon icon-target wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                        <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                            Servicios Incluidos
                        </h1> 
                        <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                            He aquí una breve lista de las actividades que experimentará
                        </p>
                    </div> 
                </div> 
            </div> 
            <div class="row"> 
                <?php if($servicios->num_rows()>0): $fragmentos = round($servicios->num_rows()/2,0) ?>
                <div class="col-md-offset-4 col-md-3 col-sm-offset-6 col-sm-6">
                    <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                        <?php for($i=0;$i<$servicios->num_rows() && $i<$fragmentos;$i++): ?>
                            <li> 
                                <h4><?= $servicios->row($i)->titulo ?></h4> 
                                <span><?= $servicios->row($i)->descripcion ?></span>
                            </li>
                        <?php endfor ?>
                    </ul> 
                </div> 
                <div class="col-md-offset-1 col-md-4 col-sm-offset-6 col-sm-6"> 
                    <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                        <?php for($i=$i;$i<$servicios->num_rows();$i++): ?>
                            <li> 
                                <h4><?= $servicios->row($i)->titulo ?></h4> 
                                <span><?= $servicios->row($i)->descripcion ?></span>
                            </li>
                        <?php endfor ?>
                    </ul> 
                </div> <!-- /.trip activities --> 
                <?php endif ?>
            </div>             
        </div> 
    </section>
    
    <section data-bg="<?= base_url() ?>images/charge-bg.jpg" class="trip-charge trip-charge3" style="background-image: url(<?= base_url() ?>images/charge-bg.jpg);"> 
        <div class="container">
            <div id="gallery" class="row" style="top:0px;"> 
                <div class="col-md-4 text-uppercase"> 
                    <div class="gallery-title"> 
                        <i class="icon icon-camera"></i> 
                        <h1>Foto Galeria</h1> 
                    </div> 
                </div> 
                <ul class="list-inline gallery-photos"> 
                    <?php foreach($this->db->get_where('hoteles_fotos',array('hoteles_id'=>$detail->id))->result() as $f): ?>
                        <li> 
                            <a href="<?= base_url('images/hoteles/'.$f->foto) ?>">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url('images/hoteles/'.$f->foto) ?>">
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul> 
            </div> 
        </div> 
    </section>
    
    
    <?= $this->load->view('includes/template/contact') ?>
    <button class="btn goUp-btn"> 
        <i class="fa fa-angle-up"></i> 
        <span>Go Up</span>
        <span class="fitz">noninfantry</span> 
    </button> <!-- /.go up arrow -->
</main>
<?= $this->load->view('includes/template/footer'); ?>
</div>