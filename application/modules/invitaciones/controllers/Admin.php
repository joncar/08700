<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function get_key(){
            $key = date("dmis").rand(0,255);
            return 'NPP-'.substr(md5($key),0,5);                
        }
        
        function invitaciones(){
            $this->norequireds = array('nro');
            $crud = $this->crud_function('','');
            $crud->field_type('nro','hidden');
            if($crud->getParameters()=='list'){
                $crud->field_type('status','dropdown',array('-1'=>'<i class="fa fa-upload fa-2x" title="Invitación Solicitada"></i>','0'=>'<span class="label label-danger">Sin Validar</span>','1'=>'<span class="label label-success">Validado</span>'));
            }else{
                $crud->field_type('status','dropdown',array('-1'=>'Solicitada','0'=>'Sin Validar','1'=>'Validado'));
            }
            $crud->order_by('id','DESC');
            $crud->callback_before_insert(function($post){
                $key = date("dmis").rand(0,255);
                $post['nro'] = get_instance()->get_key();
                return $post;
            });
            if($crud->getParameters()=='add'){
                $crud->set_rules('email','Email','required|valid_email|is_unique[invitaciones.email]');
            }
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar invitación','',base_url('boletines/sendFromId/').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar confirmación','',base_url('invitaciones/admin/confirmar').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function importar($x = '',$y = '',$z = ''){
            if($x=='procesar' && empty($z)){
                echo '<h1>¿Esta usted seguro de realizar esta acción?, esta operación sutituira los correos aún no validados y no podrá volver atras</h1>';
                echo '<p><a href="'.base_url('invitaciones/admin/importar').'">Volver Atrás</a> | <a href="'.base_url('invitaciones/admin/importar/procesar/'.$y.'/1').'">Procesar</a></p>';
            }
            elseif($x=='procesar' && $z == 1){
                $file = $this->db->get_where('excel',array('id'=>$y))->row()->fichero;
                require_once 'application/libraries/Excel/SpreadsheetReader.php';
                $excel = new SpreadsheetReader('files/'.$file);
                $data = array();
                foreach ($excel as $Row)
                {
                    $data[] = $Row;
                }
                //Empresa = 0, email = 4
                //ob_start();
                if(count($data)>0){
                    //$this->db->truncate('emails');
                    $this->db->delete('invitaciones',array('status !='=>1));
                    for($i=0;$i<count($data);$i++){
                        if(!empty($data[$i][0])){
                            if($this->db->get_where('invitaciones',array('email'=>$data[$i][0]))->num_rows()==0){
                                $this->db->insert('invitaciones',array('nro'=>$this->get_key(),'email'=>$data[$i][0]));
                                echo 'Incluido '.$data[$i][0].'<br/>';
                                ob_flush();
                            }else{
                                echo $data[$i][0].' Ya existe y esta validado<br/>';
                                ob_flush();
                            }
                        }
                    }
                    echo '<a href="'.base_url('invitaciones/admin/importar').'">Volver al backend</a>';
                }else{
                    echo "Formato incorrecto";
                }
            }else{
                $this->as['importar'] = 'excel';
                $crud = $this->crud_function('','');
                $crud->set_field_upload('fichero','files');
                $crud->add_action('<i class="fa fa-refresh"></i> Procesar','',base_url('invitaciones/admin/importar/procesar').'/');
                $crud = $crud->render();

                $this->loadView($crud);
            }
        }
        
        function confirmar($id = ''){
            if(is_numeric($id)){
                $invitacion = $this->db->get_where('invitaciones',array('id'=>$id))->row();
                $this->enviarcorreo($invitacion,2);
                $this->enviarcorreo($invitacion,1,'angels@fagepi.net');
                redirect(base_url('invitaciones/admin/invitaciones/success'));
            }
        }
    }
?>
