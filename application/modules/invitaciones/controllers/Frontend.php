<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
        }
        
        function validar(){
            $this->form_validation->set_rules('nombre','Nom','required')
                                  ->set_rules('apellido','Cognom','required')
                                  ->set_rules('invitados','número d\'assistents','required|numeric')
                                  ->set_rules('email','Email','required|valid_email')
                                  ->set_rules('nro','número de invitació','required');
            if($this->form_validation->run()){
                /*$invitacion = $this->db->get_where('invitaciones',array('nro'=>$_POST['nro'],'email'=>$_POST['email']));
                if($invitacion->num_rows()>0){
                    $invitacion = $invitacion->row();
                    if($invitacion->status==1){
                        echo 'La seva invitació ja ha estat validada anteriorment.';
                    }else{                        
                        $_POST['status'] = 1;
                        $this->db->update('invitaciones',$_POST,array('id'=>$invitacion->id));
                        $invitacion = $this->db->get_where('invitaciones',array('id'=>$invitacion->id))->row();
                        $this->enviarcorreo($invitacion,2);
                        $this->enviarcorreo($invitacion,1,'angels@fagepi.net');
                        echo 'La seva invitació s\'ha validat amb èxit. <script>setTimeout(function(){document.location.reload()},3000)</script>';
                    }
                }else{
                    echo 'El número d\'invitació no és vàlid. Comprova el teu correu per adquirir el número de la invitació, o contacta\'ns per a més informació. Recorda que has d’escriure el mateix email que has rebut l’invitació.';
                }*/
                echo 'Hem arribat a les 300 invitacions. Ja no es poden fer més reserves. Perdoneu les molèsties';
                
            }else{
                echo $this->form_validation->error_string();
            }                        
        }
        
        function solicitar(){
            $this->form_validation->set_rules('nombre','Nom','required')
                                  ->set_rules('email','Email','required|valid_email|is_unique[invitaciones.email]');
            if($this->form_validation->run()){
                $usados = 300; //Colocar en 0 y descomentar la proxima vez que se requiera usar el servicio
                //foreach($this->db->get_where('invitaciones',array('status'=>1))->result() as $u){
                /*foreach($this->db->get_where('invitaciones',array())->result() as $u){
                    $usados+= $u->invitados;
                }*/
                if(300-$usados>0){
                    $key = date("dmis").rand(0,255);
                    $key = 'NPP-'.substr(md5($key),0,5);  
                    $_POST['nro'] = $key;
                    $_POST['status'] = -1;
                    $this->db->insert('invitaciones',$_POST);
                    $this->enviarcorreo((object)$_POST,3,'info@hipo.tv');
                    echo 'La seva invitació s\'ha afegit, li enviarem un email quan sigui aprovada.';
                }else{
                    echo 'Hem arribat a les 300 invitacions. Ja no es poden fer més reserves. Perdoneu les molèsties';
                }
                //echo json_encode(array("status" => "success"));
            }else{
                echo $this->form_validation->error_string();
                //echo json_encode(array("status" => "error","type" => $this->form_validation->error_string()));
            }                        
        }
    }
?>
