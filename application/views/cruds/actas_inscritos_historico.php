<div id="extrafields" class="extrafields">
    <?php if(!empty($estudiantes)): ?>
        <div id="detalles_field_box" class="form-group">
        <label id="plan_estudio_id_display_as_box" for="field-detalles_id">
                Detalles:
        </label>
            <div>
                <div class="row">
                    <div class="col-xs-1"><b>Fila</b></div>
                    <div class="col-xs-4"><b>Alumno</b></div>
                    <div class="col-xs-1"><b>Ptos. correctos</b></div>
                    <div class="col-xs-2"><b>Calificación</b></div>
                    <div class="col-xs-1"><b>Ausente</b></div>
                    <div class="col-xs-2"><b>Observación</b></div>
                </div>
                <?php $i = 0; ?>
                <?php if(!empty($acta)): ?>                
                <?php foreach($estudiantes->result() as $n=>$d): ?>
                    <?php $detalles = !empty($acta)?$this->db->get_where('acta_final_detalle',array('incripcion_materias_id'=>$d->id,'acta_final_id'=>$acta)):'' ?>
                    <?php if(!empty($acta) && $detalles->num_rows()>0): ?>
                    <div class="row programacionMateriasPlan">
                        <div class="col-xs-1 filaNumber"><?php echo $i; $i++; ?></div>
                        <div class="col-xs-4"><?= form_dropdown_from_query('inscripcion[]',$estudiantes,'id','cedula apellido_paterno apellido_materno nombre',$d->id,'id="'.$n.'_chzn"',TRUE,'form-control materiaField','Seleccione') ?></div>
                        <div class="col-xs-1"><input name='puntos_correctos[]' type="number" class="form-control" value='<?= !empty($detalles) && $detalles->num_rows()>0?$detalles->row()->puntos_correctos:'' ?>'></div>
                        <div class="col-xs-1"><input name='calificacion[]' type="number" class="form-control" value='<?= !empty($detalles) && $detalles->num_rows()>0?$detalles->row()->calificacion:'' ?>' min="0" max="5"></div>                        
                        <div class="col-xs-2"><?= form_dropdown('ausente[]',array('0'=>'No','1'=>'Si'),!empty($detalles) && $detalles->num_rows()>0?$detalles->row()->ausente:0,'class="form-control" id="field-ausente"'); ?></div>                        
                        <div class="col-xs-2"><input name='observacion[]' type="text" class="form-control" value='<?= !empty($detalles) && $detalles->num_rows()>0?$detalles->row()->observacion:'' ?>' min="1" max="5"></div>                        
                        <div class="col-xs-1"><a href="#" class='add'><i class="fa fa-plus-circle"></i></a> <a href="#" class='remove' style="color:red"><i class="fa fa-remove"></i></a></div>
                    </div>
                    <?php endif ?>
                <?php endforeach ?>
                <?php else: ?>
                    <div class="row programacionMateriasPlan">
                        <div class="col-xs-1 filaNumber"><?= $i ?></div>
                        <div class="col-xs-4"><?= form_dropdown_from_query('inscripcion[]',$estudiantes,'id','cedula apellido_paterno apellido_materno nombre',0,'id="0_chzn"',TRUE,'form-control materiaField','Seleccione') ?></div>
                        <div class="col-xs-1"><input name='puntos_correctos[]' type="number" class="form-control" value=''></div>
                        <div class="col-xs-1"><input name='calificacion[]' type="number" class="form-control" value='' min="1" max="5"></div>
                        <div class="col-xs-2"><?= form_dropdown('ausente[]',array('0'=>'No','1'=>'Si'),0,'class="form-control" id="field-ausente"'); ?></div>                        
                        <div class="col-xs-2"><input name='observacion[]' type="text" class="form-control" value='' min="1" max="5"></div>                        
                        <div class="col-xs-1"><a href="#" class='add'><i class="fa fa-plus-circle"></i></a> <a href="#" class='remove' style="color:red"><i class="fa fa-remove"></i></a></div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    <?php else: ?>
    No existen estudiantes inscritos
    <?php endif ?>
</div>