<!-- plugins we use --> 
<script type="text/javascript" id="www-widgetapi-script" src="https://s.ytimg.com/yts/jsbin/www-widgetapi-vflh4Q3pb/www-widgetapi.js" async=""></script>
<script type="text/javascript" id="www-widgetapi-script" src="https://s.ytimg.com/yts/jsbin/www-widgetapi-vflwK39-Z/www-widgetapi.js" async=""></script>
<script src="https://www.youtube.com/iframe_api"></script>
<?php if(empty($removeFunction)): ?>
    <script src="<?= base_url() ?>js/functions.js"></script> <!-- snazzy maps -->
<?php endif ?>
   <script> 
       var tag = document.createElement('script'); 
       tag.src = "https://www.youtube.com/iframe_api"; 
       var firstScriptTag = document.getElementsByTagName('script')[0]; 
       firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
       var player; 
       function onYouTubeIframeAPIReady() { 
           player = new YT.Player('player', 
           { height: '390', 
               width: '640', 
               videoId: '_EjYquMulNM',
               rel: 0, 
               events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange } 
           }
                   ); 
       } 
       function onPlayerReady(event) { 
           event.target.playVideo();
       } 
       var done = false; 
       function onPlayerStateChange(event) { 
           if (event.data == YT.PlayerState.PLAYING && !done) {
               setTimeout(stopVideo, 0); 
               done = true; 
          }
       }
       function
       stopVideo() 
       { 
           player.stopVideo(); 
       } 
       $('#about-play').on('click', function(){ 
           player.playVideo(); 
           player.setPlaybackQuality('hd1080'); 
       }); 
       $('#about-stop').on('click', function(){ 
           player.pauseVideo() 
       }); 
    </script> <!-- initializing functions --> 
<script src="<?= base_url() ?>js/main.js"></script>