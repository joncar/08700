<!-- START - Newsletter Popup -->
<div id="somedialog" class="dialog">
    <div class="dialog__overlay"></div>
    <!-- START - dialog__content -->
    <div class="dialog__content">
        <!-- START - dialog-inner -->
        <div class="dialog-inner">
            <h4>Vol assistir al 08700?</h4>
            <p>Envia'ns el formulari i en funció de les places disponibles rebràs un email amb la conformitat de la reserva.<br><strong>Recorda que l’accés serà únicament amb invitació!</strong></p>
            <!-- START - Newsletter Form -->
            <div id="subscribe">
                <form onsubmit="return sendSolicitud(this)" id="notifyMe" method="POST">
                    <div class="form-group">
                        <div class="controls">
                            <!-- Field  -->
                            <input type="email" id="mail-sub" name="email" placeholder="Email" class="form-control email srequiredField" />
                            <!-- Spinner top left during the submission -->
                            <i class="fa fa-spinner opacity-0"></i>                            
                            <!-- Field  -->
                            <input type="text" id="mail-sub" name="nombre" placeholder="Empresa / entitat" class="form-control srequiredField nombre" style=" margin-top: 20px" />
                            <!-- Spinner top left during the submission -->
                            <!-- Field  -->
                            <input type="text" id="mail-sub" name="apellido" placeholder="Nom i cognoms" class="form-control apellido" style=" margin-top: 20px" />
                            <i class="fa fa-spinner opacity-0"></i>
                            <!-- Button -->
                            <button class="btn btn-lg submit">Sol.licitar invitació</button>
                            <div class="clear"></div>
                        </div>
                    </div>
                </form>
                <!-- Answer for the newsletter form is displayed in the next div, do not remove it. -->
                <div class="">
                    <div class="message">
                        <p class="notify-valid"></p>
                    </div>
                </div>
            </div>
            <!-- END - Newsletter Form -->
        </div>
        <!-- END - dialog-inner -->
        <!-- Cross to close the Newsletter Popup -->
        <button class="close-newsletter" data-dialog-close><i class="icon ion-android-close"></i></button>
    </div>
    <!-- END - dialog__content -->
</div>
<!-- END - Newsletter Popup -->
<script>
function sendSolicitud(form){
    form = new FormData(form);
    $.ajax({
        url:'<?= base_url('invitaciones/frontend/solicitar') ?>',
        data:form,
        processData:false,
        type:'POST',
        cache:false,
        context:document.body,
        contentType:false,
        success:function(data){
            $(".notify-valid").html(data);
            $(".message").show();
        }
    });
    return false;
}
</script>