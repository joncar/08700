<!-- START - Newsletter Popup -->
<div id="validacion" class="dialog">
    <div class="dialog__overlay"></div>
    <!-- START - dialog__content -->
    <div class="dialog__content">
        <!-- START - dialog-inner -->
        <div class="dialog-inner">
            <h4>La seva validació</h4>
            <div class="response">
                <p></p>
            </div>
            <!-- END - Newsletter Form -->
        </div>
        <!-- END - dialog-inner -->
        <!-- Cross to close the Newsletter Popup -->
        <button class="close-newsletter" data-dialog-close><i class="icon ion-android-close"></i></button>
    </div>
    <!-- END - dialog__content -->
</div>
<!-- END - Newsletter Popup -->