<section data-bg="<?= base_url() ?>images/mountains_bg.jpg" class="contacts-section" style="background-image: url(<?= base_url() ?>images/mountains_bg.jpg);">
    <div class="container text-center"> 
        <div class="row"> 
            <div class="col-md-12">
           <div class="user-avatar wow slideInDown" data-wow-delay="0.1s" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; -webkit-animation-duration: 1s; animation-delay: 0.1s; -webkit-animation-delay: 0.1s; animation-name: slideInDown; -webkit-animation-name: slideInDown;">
                    <img class="img-responsive" alt="user-avatar" src="<?= base_url() ?>images/avatar.jpeg"> 
                </div> 
                <h1 class="text-uppercase">Hablemos!</h1>
            </div>
        </div> 
        <div class="row"> 
            <div class="col-md-12"> 
                <p style="margin: 0px;">Si le ha quedado alguna consulta al respecto o desea que le ampliemos la información de algún punto en concreto respecto al viaje de su hijo, no dude en llamar a nuestras operadoras quiénes estarán encantadas de atenderle:
                
</p>
            </div>
        </div>
        <div class="row">
            <ul class="col-md-12 text-uppercase list-inline contacts-list"> 
                <li data-wow-delay="0.1s" class="wow fadeIn" style="animation-delay: 0.1s; animation-name: none;"> 
                    <a href="https://www.facebook.com/kanvoy1/"> 
                        <i class="icon icon-facebook"></i> 
                        <span>Facebook</span>
                        <span class="divc">noninfantry</span>
                    </a>
                </li>
                <li data-wow-delay="0.3s" class="wow fadeIn" style="animation-delay: 0.3s; animation-name: none;"> 
                    <a href="tel:123-456-003"> 
                        <i class="icon icon-phone"></i>
                        <span>881 992303</span>
                        <span class="divc">exudative</span>
                    </a>
                </li>
                <li data-wow-delay="0.5s" class="wow fadeIn" style="animation-delay: 0.5s; animation-name: none;"> 
                    <a href="mailto:info@miex.me">
                        <i class="icon icon-envelope"></i> 
                        <span>info@miex.me</span>
                    </a>
                </li> 
            </ul> 
        </div> 
        <div class="row"> 
            <div class="col-md-12 text-uppercase text-center"> 
                <a data-wow-delay="0.2s" class="btn book-btn wow slideInUp" href="<?= base_url('p/contactenos') ?>" style="animation-delay: 0.2s; animation-name: none;">Contrátalo ahora</a>
            </div> 
        </div>
    </div>
</section> <!-- go up arrow --> 
