<footer class="footer">
    <div class="container">
        <div class="row overflow">
            <div class="col-md-3 col-sm-6 wow fadeInUp">
                <h4 class="text-capitalize footer-col-title">Últimos artículos</h4>
                <ul class="list-unstyled text-uppercase footer-blog-intro">
                    <?php 
                        $this->db->order_by('id','DESC');
                        $this->db->limit('3');
                    ?>
                    <?php foreach($this->db->get('blog')->result() as $b): ?>
                        <li><a href="<?= site_url('blog/'.toURL($b->id.'-'.$b->titulo)) ?>"><?= $b->titulo ?></a></li>
                    <?php endforeach ?>
                </ul>
                  <p style=" color: rgb(255, 255, 255); font-size: 10px; font-weight: bold; letter-spacing: 2px; margin: 0px; font-family: raleway, sans-serif; text-transform: uppercase; color: #f72859"> <a href="javascript:legal()" title="">Aviso legal</a></p>
            </div>
            
            <div class="col-md-3 col-sm-6 wow fadeInUp">
                <form id="subscribeForm" class="form" onsubmit="return subscribir()">
                    <input type="email" id="email" name="email" placeholder="Tu email..."> 
                    <input type="submit" name="submit" value="Subcribete">
                </form>
                <ul class="list-inline social-links">
                    <li><a href="https://www.facebook.com/kanvoy1/" class="fb-lnk"> <i class="fa fa-facebook-official"></i> </a></li>
                    <li><a href="https://twitter.com/_kanvoy" class="tw-lnk"> <i class="fa fa-twitter-square"></i> </a></li>
                    <li><a href="https://www.instagram.com/_kanvoy/" class="inst-lnk"> <i class="fa fa-instagram"></i> </a></li>
                    <li><a href="https://www.youtube.com/channel/UCMpBfRuhSaCajp2--XAASKw" class="tub-lnk"> <i class="fa fa-youtube-square"></i> </a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeInUp">
                <h4 class="text footer-col-title">Teléfono Exclusivo Padres</h4>
                 <p style=" color: #fff; font-size: 16px; font-weight: bold; letter-spacing: 2px;">881 992 303</p> </br>
                <div class="footer-about">
                    <h4 class="text footer-col-title">Horario de Atención al Cliente</h4>
                    <p style=" color: rgb(255, 255, 255); font-size: 10px; font-weight: bold; letter-spacing: 2px; margin: 0px; font-family: raleway, sans-serif; text-transform: uppercase;">lunes a viernes de 10:00 a 13:30h. </br>y de 17:00 a 20:00h.</p>                                        
                    <h4 class="text footer-col-title">Dirección</h4>
                    <p style=" color: rgb(255, 255, 255); font-size: 10px; font-weight: bold; letter-spacing: 2px; margin: 0px; font-family: raleway, sans-serif; text-transform: uppercase;">C/ Condes de Andrade nº 1 L.1</br>CULLEREDO, A Coruña. C.P.15174</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeInUp">
                <h4 class="text-capitalize footer-col-title">links</h4>
                <ul class="list-unstyled text-uppercase footer-links">
                    <li><a href="<?= base_url('p/quienes-somos') ?>">quienes somos</a></li>
                    <li><a href="<?= base_url('hoteles') ?>">Hoteles</a></li>
                    <li><a href="<?= base_url('p/sobre-nosotros') ?>">Sobre MIEX</a></li>
                    <li><a href="<?= base_url('blog') ?>">Blog</a></li>
                    <li><a href="<?= base_url('p/contactenos') ?>">Contacto</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center text-uppercase">
                <div class="copyright">
                    <small>
                        <small>
                            <a href="">Made in BCN with love </a>
                        </small>
                    </small>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php $this->load->view('includes/scripts'); ?>
<script>
    function subscribir(){
        $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:$("#email").val()},function(data){
            emergente(data);
        });
        return false;
    }
    
    function legal(){
        $.post('<?= base_url() ?>main/mostrarlic',{},function(data){
                 emergente(data);
        });
    }

</script>

