<canvas id="canvasbg"></canvas>
<canvas id="canvas"></canvas>
<!-- Page preloader -->
<div id="loading">
    <div id="preloader">
        <span></span>
        <span></span>
    </div>
</div>
<!-- START - Home part -->
<div class="outer-home">
    <!-- START - Home section -->
    <section id="home">
        <!-- Background picture(s) -->
        <div id="vegas-background"></div>
        <!-- Overlay -->
        <div class="global-overlay"></div>
        <!-- Your logo -->
        <div class="row" style="margin-left:0px; margin-right: 0px;">
            <div  class="col-xs-12 col-sm-4"  style="margin-top:px">
                <img src="img/logo.svg" alt="" class="brand-logo text-intro opacity-0" />
            </div>
            <nav class="navbuttontop col-xs-12 col-sm-offset-2 col-sm-6 col-md-offset-3 col-md-5 col-lg-offset-8 col-lg-4" >
                <ul class="row buttonstop" style=" margin-left: 22px;">
                    <li>
                        <a href="#" id="open-more-info" data-target="right-side" class="light-btn text-intro opacity-0" style="width:100%; max-width:100%; text-align:center;">L'esdeveniment</a>
                    </li>
                    <li>
                        <a data-dialog="somedialog" class="action-btn trigger text-intro opacity-0" style="width:100%; max-width:100%; text-align:center;">Demana la invitació</a>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- START - Content -->
        <div class="content">
            <h1 class="text-intro opacity-0">La Gran nit en imatges <br></h1>
            <p class="text-intro opacity-0">Dijous 23 de febrer 2017</p>  
            
            <div class="row" style="margin-top:5px;">
                <!-- START - Gallery -->
                <div class="col-xs-12 col-sm-4 col-lg-4 photo-line">
                    <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                        <!-- START - Gallery item -->
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6 col-lg-offset-4 col-lg-6">
                            <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                            <a class="box-picture" href="img/FC13.jpg" itemprop="contentUrl" data-size="1920x1280">
                                <!-- Your picture -->
                                <img src="img/FC13.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                                <p>Veure</p>
                            </a>
                            <!-- Picture's description below this one -->
                            <figcaption itemprop="caption description">
                                <div class="photo-details">
                                    <h5 style=" margin-top: 11px">Benvinguda i Fotocall</h5>
                                </div>
                            </figcaption>

                        </figure>  


                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC1.jpg" xxxxxxxx itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC2.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC3.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC3.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC4.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC4.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC5.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC5.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC6.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC6.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC7.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC7.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC8.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC8.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC9.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC9.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC10.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC10.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC11.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC11.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC12.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC12.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC13.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC13.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC14.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC14.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC15.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC15.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC17.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC17.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC18.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC18.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC19.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC19.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC20.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC20.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC21.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC21.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC22.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC22.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC23.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC23.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC24.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC24.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC25.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC25.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC26.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC26.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC27.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC27.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC28.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC28.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC29.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC29.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC31.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC31.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC32.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC32.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC33.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC33.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC34.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC34.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC35.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC35.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC36.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC36.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC37.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC37.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC38.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC38.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC39.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC39.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC40.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC40.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC41.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC41.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC42.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC42.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC43.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC43.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC44.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC44.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC45.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC45.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC47.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC47.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC48.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC48.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC46.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC46.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/FC16.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/FC16.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>


                    </div>

                </div>

                <!-- START - Gallery -->
                <div class="col-xs-12 col-sm-4 col-lg-4  photo-line">
                    <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                        <!-- START - Gallery item -->
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6  col-lg-offset-4 col-lg-6">
                            <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                            <a class="box-picture" href="img/expo1.jpg" itemprop="contentUrl" data-size="1920x1280">
                                <!-- Your picture -->
                                <img src="img/expo1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                                <p>Veure</p>
                            </a>
                            <!-- Picture's description below this one -->
                            <figcaption itemprop="caption description">
                                <div class="photo-details">
                                    <h5 style=" margin-top: 11px">Exposició Productes Creatius</h5>
                                </div>
                            </figcaption>

                        </figure>  


                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/expo1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/expo1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/expo2.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/expo2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/expo3.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/expo3.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/expo4.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/expo4.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>

                    </div>

                </div>

                <!-- START - Gallery -->
                <div class="col-xs-12 col-sm-4 col-lg-4 photo-line">
                    <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                        <!-- START - Gallery item -->
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6  col-lg-offset-4 col-lg-6">
                            <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                            <a class="box-picture" href="img/sala1.jpg" itemprop="contentUrl" data-size="1920x1280">
                                <!-- Your picture -->
                                <img src="img/sala1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                                <p>Veure</p>
                            </a>
                            <!-- Picture's description below this one -->
                            <figcaption itemprop="caption description">
                                <div class="photo-details">
                                    <h5 style=" margin-top: 11px">Sala</h5>
                                </div>
                            </figcaption>

                        </figure>  


                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/sala1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/sala1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/sala2.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/sala2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                    

                    </div>

                </div>
            </div>
            <div class="row" style="margin-top:5px;">
                <!-- START - Gallery -->
                <div class="col-xs-12 col-sm-4 col-lg-4 photo-line">
                    <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                        <!-- START - Gallery item -->
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6  col-lg-offset-4 col-lg-6">
                            <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                            <a class="box-picture" href="img/desfile8.jpg" itemprop="contentUrl" data-size="1920x1280">
                                <!-- Your picture -->
                                <img src="img/desfile48.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                                <p>Veure</p>
                            </a>
                            <!-- Picture's description below this one -->
                            <figcaption itemprop="caption description">
                                <div class="photo-details">
                                    <h5 style=" margin-top: 11px">Desfilada</h5>
                                </div>
                            </figcaption>

                        </figure>  


                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile2.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile3.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile3.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile4.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile4.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile5.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile5.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile6.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile6.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile7.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile7.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile8.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile8.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile9.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile9.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile10.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile10.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile11.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile11.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile12.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile12.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile13.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile13.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile14.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile14.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile15.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile15.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile16.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile16.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile17.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile17.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile18.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile18.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile19.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile19.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile20.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile20.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile21.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile21.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile22.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile22.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile23.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile23.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile24.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile24.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile25.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile25.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile26.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile26.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile27.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile27.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile28.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile28.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile29.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile29.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile30.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile30.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile31.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile31.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile32.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile32.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile33.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile33.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile34.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile34.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile35.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile35.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile36.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile36.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile37.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile37.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile38.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile38.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile39.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile39.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile40.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile40.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile41.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile41.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile42.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile42.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile43.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile43.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile44.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile44.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile45.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile45.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile48.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile48.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile49.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile49.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile50.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile50.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile51.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile51.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile52.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile52.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile53.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile53.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile54.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile54.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile55.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile55.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile56.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile56.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile57.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile57.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile58.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile58.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile59.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile59.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile60.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile60.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile62.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile62.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile64.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile64.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile65.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile65.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile66.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile66.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile67.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile67.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile68.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile68.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile69.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile69.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile70.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile70.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile71.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile71.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/desfile75.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/desfile75.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>

                    </div>

                </div>

                <!-- START - Gallery -->
                <div class="col-xs-12 col-sm-4 col-lg-4 photo-line">
                    <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                        <!-- START - Gallery item -->
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6  col-lg-offset-4 col-lg-6 ">
                            <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                            <a class="box-picture" href="img/sopar1.jpg" itemprop="contentUrl" data-size="1920x1280">
                                <!-- Your picture -->
                                <img src="img/sopar1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                                <p>Veure</p>
                            </a>
                            <!-- Picture's description below this one -->
                            <figcaption itemprop="caption description">
                                <div class="photo-details">
                                    <h5 style=" margin-top: 11px">Sopar i actuació</h5>
                                </div>
                            </figcaption>

                        </figure>  


                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/sopar1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/sopar1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/concert1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/concert1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/concert2.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/concert2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/concert3.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/concert3.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/concert4.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/concert4.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>
                    

                    </div>

                </div>

                <!-- START - Gallery -->
                <div class="col-xs-12 col-sm-4 col-lg-4 photo-line">
                    <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                        <!-- START - Gallery item -->
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6  col-lg-offset-4 col-lg-6">
                            <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                            <a class="box-picture embed-responsive embed-responsive-4by3" href="#" itemprop="contentUrl" data-size="1920x1280" style=" padding-bottom: 66%;">
                                <!-- Your picture -->
                                <iframe src="https://www.youtube.com/embed/EuLB547UoDY"></iframe>
                            </a>
                            <!-- Picture's description below this one -->
                            <figcaption itemprop="caption description">
                                <div class="photo-details">
                                    <h5 style=" margin-top: 11px">Video</h5>
                                </div>
                            </figcaption>

                        </figure>  


                        
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                           <a class="box-picture embed-responsive embed-responsive-4by3" href="https://www.youtube.com/embed/EuLB547UoDY" itemprop="contentUrl" data-size="1920x1280" style=" padding-bottom: 66%;">
                                <!-- Your picture -->
                                <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/EuLB547UoDY?theme=light&showinfo=0&modestbranding=0" frameborder="0" allowfullscreen></iframe>
                 
                                
                            </a>
                        </figure>
                        <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <a class="box-picture" href="img/video1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/video1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                        </figure>

                    </div>

                </div>
            </div>
            
            <div class="row bottominnerhome">
                <div class="bottomhome">
                    <div>
                        <div class="col-xs-12 col-sm-4 colaboradores">
                            Organitzadors <br/>
                            <img src="img/logos2.svg">
                             
                        </div>
                        
                        <div class="col-xs-12 col-sm-4 colaboradores">
                            Col.laboradors <br/>
                            <img src="img/logos.svg">
                        </div>
                        <div class="col-xs-12 col-sm-4 socialicons">
                            <div>
                                <a href="https://www.facebook.com/Agrupació-Tèxtil-FAGEPI-309587069085/?fref=ts"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/FAGEPI"><i class="fa fa-twitter"></i></a>
                          
                            </div>
                            <div class="copy">
                                © 08700 - Fet i desenvolupat a Igualada
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="bottomouterhome">
                <div class="bottomhome">
                    <div>
                        <div class="col-xs-12 col-sm-4 colaboradores">
                            Organitzadors <br/>
                            <img src="img/logos2.svg">
                             
                        </div>
                        <div class="col-xs-12 col-sm-4 colaboradores">
                        Col.laboradors <br/>
                        <img src="img/logos.svg">
                         </div>
                        <div class="col-xs-10 col-sm-4 socialicons">
                            <div>
                                <a href="https://www.facebook.com/Agrupació-Tèxtil-FAGEPI-309587069085/?fref=ts"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/FAGEPI"><i class="fa fa-twitter"></i></a>
                             
                            </div>
                            <div class="copy">
                                © 08700 - Fet i desenvolupat a Igualada
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- END - Home section -->
</div>
<!-- END - Home part -->
<script>
function sendNro(form){
    form = new FormData(form);
    $.ajax({
        url:'<?= base_url('invitaciones/frontend/validar') ?>',
        data:form,
        processData:false,
        type:'POST',
        cache:false,
        context:document.body,
        contentType:false,
        success:function(data){
            $(".response").html(data);
            dlg2.toggle();
        }
    });
    return false;
}
console.log("Marico");
</script>
