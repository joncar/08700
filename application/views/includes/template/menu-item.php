<ul class="nav navbar-nav text-uppercase text-center"> 
    <li class="animated fadeInDown">
        <a href="<?= base_url('p/quienes-somos') ?>">Quienes Somos <span class="sr-only">(current)</span><span class="fitz">vindicated</span></a>        
    <li class="animated fadeInDown">
        <a href="<?= base_url('p/sobre-nosotros') ?>">Sobre Miex</a>
    </li>
    <li class="animated fadeInDown">
        <a href="<?= base_url('hoteles') ?>">Hoteles</a>
    </li> 
    <li class="animated fadeInDown">
        <a href="<?= base_url('blog') ?>">Blog</a>
    <li class="animated fadeInDown">
        <a href="<?= base_url('p/contactenos') ?>">Contáctanos</a>
    </li> 
    <li class="animated fadeInDown" style="color:black">
        <a href="<?= base_url('panel') ?>" style="color:black !important">Zona Usuarios</a>
    </li>
    <?php if(!empty($_SESSION['user'])): ?>
    <li class="animated fadeInDown" style="color:black">
        <a href="<?= base_url('main/unlog') ?>" style="color:black !important">Salir</a>
    </li>
    <?php endif ?>
</ul> 