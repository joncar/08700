<!-- navigation / main menu --> 
<nav class="navbar navbar-fixed-top"> 
    <div class="container"> 
        <!-- Brand and toggle get grouped for better mobile display --> 
        <div class="navbar-header"> 
            <button aria-expanded="false" data-target="#myMenu" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
            </button> 
            <a href="<?= base_url() ?>" class="navbar-brand">
                <img src="<?= base_url() ?>images/logopetit.png" style="width:180px">
            </a>
            <p class="lehi" style="margin: 0px;">autointoxication</p>
            <span class="classy">thiobacilli personifying</span> 
        </div> <!-- Collect the nav links, forms, and other content for toggling --> 
        <div id="myMenu" class="collapse navbar-collapse"> 
            <?= $this->load->view('includes/template/menu-item') ?>
        </div><!-- /.navbar-collapse --> 
    </div><!-- /.container --> 
</nav> 