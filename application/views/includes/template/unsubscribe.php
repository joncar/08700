<?php if(!empty($success)): ?>
    <div class="alert alert-success">Se ha dado de baja satisfactoriamente, ya no le llegaran los correos informativos a su correo electronico</div>
<?php else: ?>
    <?php if(isset($success)): ?>
        <div class="alert alert-danger">El correo que desea dar de baja no se encuentra registrado</div>
    <?php endif ?>
        <div class="well">
            <form onsubmit="return confirm('Esta usted seguro que desea dar de baja su correo?')" method="post">
                <div class="form-group">
                    <label for="email" style="color:white;">Email:</label>
                    <input type="email" name="email" class="form-control">
                </div>
                <button type="submit" class="btn btn-success">Donar de baixa</button>
            </form>
        </div>
<?php endif; ?>
