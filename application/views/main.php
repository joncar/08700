<?php $this->load->view('includes/template/header'); ?>
<!-- Transparent div used when more info part is opening, allow the user to click on the home to get back on it -->
<div class="close-right-part layer-left hide-layer-left"></div>
<!-- Skew left border info part -->
<div class="border-right-side hide-border"></div>
<!-- START - More Informations part -->
<section id="right-side" class="hide-right">
    <!-- START - Content -->
    <div class="content">
        <h2>08700 Nits de Pell i Punt.</h2>
        <p>Les indústries tèxtil-moda i de l’adob de pell han estat protagonistes del desenvolupament econòmic de la comarca de l’Anoia des del segle XI. La transformació permanent de la indústria en els darrers 400 anys ha descansat sobre les modificacions que s’han produït en les demandes del mercat, la innovació tecnològica i els canvis que les regulen. Les persones, fins i tot famílies senceres, implicades en aquests dos sectors han contribuït, amb el seu talent i esforç, al desenvolupament econòmic i social de la comarca de l’Anoia. Igualada, com a capital de comarca, manté unes capacitats diferencials en l’àmbit de les “Indústries del Disseny”. Aquest àmbit comprèn tots aquells sectors que tenen el disseny com a element transversal en el seu procés de creació i desenvolupament de productes. El tèxtil-moda i l’adob de pell són els sectors més representatius de les indústries del disseny a la nostra comarca: S’hi agrupen més de 200 empreses, que ocupen a 2.200 professionals i es genera una xifra agregada de negoci superior als 320 milions d’euros. Les indústries del disseny són una font de riquesa present i una palanca de creixement futur del nostre territori. La “08700 - Nits de Pell i Punt” és la primera edició d’un esdeveniment lúdic i cultural impulsat per el Consorci de Comerç Artesania i Moda de Catalunya, la Diputació de Barcelona, l’Ajuntament d’Igualada i l’Agrupació Tèxtil Fagepi, un esdeveniment que pretén connectar Igualada amb la passarel•la de moda 080 Barcelona Fashion i reivindicar el talent de la nostra gent, la creativitat de les nostres marques i la capacitat industrial de l’Anoia com una part central de la Catalunya creativa.</p>
        <br><br>
        <div class="row">
            <!-- START - Box info -->
            <div class="col-xs-12 col-sm-12 col-lg-6 box-info">
                <div class="box-info-light equalizer">
                    <span class="icon"><i class="icon ion-ios-calendar-outline"></i></span>
                    <h3> PRIMERA EDICIÓ DE LES NITS DE PELL I PUNT</h3>
                   <p style="font-weight: 600; font-size: 1.3em">Dijous 23 de febrer a les 20:30h</p>
                    <p>El Consorci de Comerç Artesania i moda de Catalunya, en col•laboració amb la Diputació de Barcelona i l’Ajuntament d’Igualada us convida el 
                    proper 23 de febrer a les 20:30 h a la primera edició de “Les Nits de Pell i Punt”  a l’antic teatre Mercantil d’Igualada (La Sala).<strong></strong> <a href="https://goo.gl/maps/iL6pYaccn9q" class="phone-mail-link">Mapa  </a><i class="icon ion-ios-location"></i> </p>
                <img src="img/gallery-11.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" style=" margin-top: 40px; width: 100%" />
                </div>
            </div>
            <!-- END - Box info -->
            <!-- START - Box info -->
            <div class="col-xs-12 col-sm-12 col-lg-6 box-info">
                <div class="box-info-dark equalizer" >
                    <span class="icon"><i class="icon ion-ios-color-wand-outline"></i></span>
                    <h3 class="text-uppercase color-grey">PER QUÈ UNA NIT DE PELL I PUNT A IGUALADA?</h3>
                    <p>Aquesta iniciativa pretén apropar l’esdeveniment de moda de referència de Catalunya, la 080 BARCELONA FASHION, a la ciutat d’Igualada i promocionar així les principals indústries del disseny de la comarca de l’Anoia -el gènere de punt i la pell-.  </p>
                <img src="img/gallery-12.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" style=" margin-top: 70px; width: 100%" />
                </div>
            </div>
            <!-- END - Box info -->
            <!-- START - Box info -->
            <div class="col-xs-12 col-sm-12 col-lg-6 box-info" style=" width: 100%">
                <div class="box-info-light equalizer" style=" height: 453px; margin-top: 35px;">
                    <span class="icon"><i class="icon ion-ios-calendar-outline"></i></span>
                    <h3>EXPOSICIÓ DE PRODUCTES CREATIUS I INNOVADORS DISSENYATS I PRODUÏTS A IGUALADA.</h3>
                   <p style="font-weight: 600; font-size: 1.3em">Al mateix recinte</p>
                    <p>El centre Tecnològic FITEX, el clúster Europeu de la Pell d’Igualada, L’Escola d’Arts i Oficis Gaspar Camps, alumnes i emprenedors anoiencs exposaran alguns productes creatius i innovadors elaborats al nostre territori.  </p>
                <img src="img/gallery-0.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" style=" margin-top: 40px" />
                </div>
            </div>
            <!-- END - Box info -->
        </div>
        <span class="separator"></span>
        <h3>20:30 h Desfilada de nou talent emergent</h3>
        <!-- START - Gallery -->
        <div class="photo-line">
            <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                <!-- START - Gallery item -->
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6 col-lg-6">
                    <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                    <a class="box-picture" href="img/gallery-1.jpg" itemprop="contentUrl" data-size="1920x1280">
                        <!-- Your picture -->
                        <img src="img/gallery-1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                        <p>Veure</p>
                    </a>
                    <!-- Picture's description below this one -->
                    <figcaption itemprop="caption description">
                        <div class="photo-details">
                            <h3>ZER</h3>
                            <h5>Núria Costa Ginjaume i Ane Castro Sudupe. Igualada</h5>
                            <p>La Núria Costa Ginjaume i l’Ane Castro Sudupe, dues identitats que es complementen en una de sola, han creat al 2016 la marca ZER. ZER entén el disseny de moda con una experiència que proposa la transformació de la societat mitjançant la creació i la combinació de coneixements de procedència molt diversa. Construïm així un concepte que anomenem “la segona democratització de la moda”, en el sentit de reivindicar la individualitat de cada ésser. Ens trobem en una societat globalitzada on la individualitat passa desapercebuda. És per tot això que pretenem impulsar el plantejament de problemes reals, el debat social i, a la vegada, els espais de reflexió personal que ens apropin als destinataris dels nostres disseny, sense oblidar la tecnologia com un aspecte rellevant en la cultura contemporània.
</p>
                        </div>
                    </figcaption>
                </figure>  
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
           </div>
       </div>
        
        <!-- START - Gallery -->
        <div class="photo-line">
            <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                <!-- START - Gallery item -->
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6 col-lg-6">
                    <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->                    
                    <a class="box-picture" href="img/gallery-2.jpg" itemprop="contentUrl" data-size="1920x1280">                                              
                        <!-- Your picture -->
                        <img src="img/gallery-2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                        <p>Veure</p>
                    </a>
                    <!-- Picture's description below this one -->
                    <figcaption itemprop="caption description">
                        <div class="photo-details">
                            <h3>Alicia González</h3>
                            <h5>ODD ONE OUT. Barcelona</h5>
                            <p>Tan bon punt es graduà en Enginyeria Superior en Informàtica per la UPC, l'Alicia decideix fer realitat el seu somni, graduant-se en Disseny de Moda per LCI Barcelona. Finalista en diferents concursos com Bread&Butter Graduation Tour i 'Pasarela Fortuny' a Granada, va ampliar la seva formació amb estades als departaments de disseny de marques com Josep Font, Lydia Delgado i Guillermina Baeza amb una forta aposta per la qualitat, el detall en els acabats i l'artesania. Però és durant les seves pràctiques al departament de qualitat de tricot a MANGO, on descobreix la seva passió: el punt. A ODD ONE OUT, frase feta de l'anglès que es podria traduir com 'el que no encaixa', l'Alicia plasma la seva devoció pel punt fent ús de fibres nobles i tècniques artesanals. ODD ONE OUT és més que una marca de moda, és un concepte de vida que la dissenyadora defineix com 'urban luxury' i que es caracteritza per la qualitat, l'exclusivitat i l'atemporalitat; essent ‘Made to Last’ el seu leitmotiv. Totes les peces de les seves col•leccions estan dissenyades i teixides per ella mateixa, ja sigui a mà o fent ús d'antigues tricotoses manuals que ha recuperat i en les quals s'ha format de manera autodidacta. <a href="http://www.theoddoneoutdesign.com" class="phone-mail-link"> www.theoddoneoutdesign.com</a>
</p>
                        </div>
                    </figcaption>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-2_1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-2_1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-2_2.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-2_2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-2_3.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-2_3.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-2_4.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-2_4.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
           </div>
       </div>
        
        
        
        <!-- START - Gallery -->
        <div class="photo-line">
            <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                <!-- START - Gallery item -->
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6 col-lg-6">
                    <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                    <a class="box-picture" href="img/gallery-3.jpg" itemprop="contentUrl" data-size="1920x1280">
                        <!-- Your picture -->
                        <img src="img/gallery-3.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                        <span class="">
                            <span class="">
                                <i class=""></i>
                            </span>
                        </span>
                        <p>Veure</p>
                    </a>
                   <!-- Picture's description below this one -->
                    <figcaption itemprop="caption description">
                        <div class="photo-details">
                           <h3>Xavi Grados</h3>
                            <h5>Igualada</h5>
                            <p>En Xavi Grados va néixer a Igualada l’any 1992. Graduat en disseny de moda per l’Escola Llotja de Barcelona al juny de 2014, va ser semifinalista als premis ModaFAD del mateix any amb una innovadora col•lecció de punt. En Xavi Grados pretén fer bandera de la ciutat que l'ha vist créixer, treballant el punt i la pell en les seves peces i garantint la millor qualitat en primeres matèries per poder fer, d'aquesta manera, un producte amb denominació d'origen igualadí. Les dues primeres col•leccions (tardor/hivern i primavera/estiu) del jove dissenyador van debutar a la reconeguda passarel•la 080 Barcelona Fashion en les dues edicions de l’any 2016, començant així el recorregut d’en Xavi Grados per les passarel•les internacionals.   <a href="http://www.xavigrados.com" class="phone-mail-link">www.xavigrados.com</a>
                        </div>
                    </figcaption>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-3_1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-3_1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-3_2.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-3_2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-3_3.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-3_3.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-3_4.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-3_4.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
           </div>
       </div>
        
        
        
        <!-- START - Gallery -->
        <div class="photo-line">
            <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                <!-- START - Gallery item -->
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6 col-lg-6">
                    <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                    <a class="box-picture" href="img/gallery-4.jpg" itemprop="contentUrl" data-size="1920x1280">
                        <!-- Your picture -->
                        <img src="img/gallery-4.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                        <span class="">
                            <span class="">
                                <i class=""></i>
                            </span>
                        </span>
                        <p>Veure</p>
                    </a>
                    <!-- Picture's description below this one -->
                    <figcaption itemprop="caption description">
                       <div class="photo-details">
                            <h3>Pelleteria VM La Sibèria</h3>
                            <h5>Barcelona</h5>
                            <p>Membres de la quarta generació de la pelleteria VM La Sibèria, que va ser fundada l’any 1891. Economistes de formació i pelleters de professió, des de joves han après l’ofici de confeccionar la pell als tallers de la casa i sota l’experiència dels pares i de la resta de professionals tècnics que formen l’equip. Coneixedors dels diferents processos de tractament de la pell, sempre han unit la feina ben feta, gairebé artesanal, amb la innovació i el disseny. Des de fa més de 10 anys són al capdavant de la direcció de l’empresa. Entre les diverses tasques que desenvolupen hi ha les de disseny, elecció de materials acabats i control de la producció. <a href="http://www.pelleterialasiberia.com" class="phone-mail-link">www.pelleterialasiberia.com</a>
                        </div>
                    </figcaption>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-4_1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-4_1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-4_2.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-4_2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-4_3.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-4_3.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-4_4.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-4_4.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
           </div>
       </div>
       
       <span class="separator"></span>
       <h3>A continuació sopar a peu dret gaudint de productes de proximitat</h3> 
       <div class="photo-line">
            <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                <!-- START - Gallery item -->
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6 col-lg-6">
                    <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                    <a class="box-picture" href="img/gallery-20.jpg" itemprop="contentUrl" data-size="1920x600">
                        <!-- Your picture -->
                        <img src="img/gallery-20.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />

           </div>
       </div>
       <span class="separator"></span>
       <h3>22 h Concert de Pau Sastre</h3> 
       <div class="photo-line">
            <div class="my-gallery row" itemscope itemtype="http://schema.org/ImageGallery">
                <!-- START - Gallery item -->
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-xs-12 col-sm-6 col-lg-6">
                    <!-- Link to the picture and to open the gallery / Fill up well the data-size property -->
                    <a class="box-picture" href="img/gallery-6.jpg" itemprop="contentUrl" data-size="1920x1280">
                        <!-- Your picture -->
                        <img src="img/gallery-6.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" />
                        <p>Veure</p>
                    </a>
                    <!-- Picture's description below this one -->
                    <figcaption itemprop="caption description">
                        <div class="photo-details">
                           <h3>Pau Sastre</h3>
                            <h5>Igualada</h5>
                            <p>En Pau Sastre és un artista amb un gran toc d’elegància. Poc amic d’etiquetes, cultiva un estil fresc, comunicatiu i decididament eclèctic on es passeja des del pop fins al funk, aturant-se al hip hop o al jazz. Productor, compositor i multi–instrumentista, format a Berklee College of Music, de Boston, llicenciat en Producció Musical i Composició de Jazz, Pau Sastre acaba de llançar el seu tercer treball discogràfic “Tan Bons Com Puguem Ser”. Pau Sastre neix el 3 de desembre de 1974 a Badalona. Amb tan sols 8 anys inicià els estudis de solfeig i piano al conservatori de música d’Igualada. Als 16 anys comença la seva carrera professional, actuant al costat de diferents grups locals. Més tard, al finalitzar els estudis musicals a l’Aula de Música Moderna i Jazz de Barcelona, li concedeixen una beca, gràcies a la qual podrà viatjar i formar-se a la Berklee College of Music, de Boston. Un cop llicenciat en composició de jazz i producció i enginyeria musical torna a Espanya i comença a exercir com a compositor, arranjador i productor. L’any 2006 edita el seu primer treball discogràfic anomenat “HighlyAddictive” (SatchmoRecords) i en ell es fonen estils com el Funk, el Pop i el Jazz. Al marge de la seva carrera com a artista continua exercint com a productor musical, treballant per a diversos artistes del panorama musical català. També col•labora al costat de Josep Mas “Kitflus”, produint la música per a diversos programes de televisió nacionals. Des de l’any 2008 i fins al 2013, en Pau Sastre va formar part de la banda que acompanya en David Bustamante en directe tocant la guitarra acústica i als cors. La cançó “Saber Perder” inclosa en el seu penúltim àlbum “Mío” va ser escrita conjuntament pels dos artistes. Pau Sastre ha format part també de formacions com Jockers, Talking Rabitts, Guru, Sexy Phonics, BCN Division, Gospel Colours, Gospelising i Jorge Salán. <a href="http://www.pausastre.com" class="phone-mail-link">www.pausastre.com</a></p>
            <div class="row"><iframe width="100%" height="315" src="https://www.youtube.com/embed/fN3znctEHRo" frameborder="0" allowfullscreen></iframe></div>
                        </div>
                    </figcaption>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-6_1.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-6_1.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-6_2.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-6_2.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-6_3.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-6_3.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <figure style="display:none" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="box-picture" href="img/gallery-6_4.jpg" itemprop="contentUrl" data-size="1920x1280"><!-- Your picture --><img src="img/gallery-6_4.jpg" itemprop="thumbnail" alt="This is my work" class="img-responsive" /></a>
                </figure>
                <!-- END - Gallery item -->
           </div>
       </div>
       
       span class="separator"></span>
        <h2>Tens algun suggeriment o dubte?</h2>

        <p>Restem a la teva disposició de  <strong>dilluns a divendres</strong>, de 9:00 AM a 13:00 PM i de 16:00 PM a 19:00 PM.<br>
            Contacta amb nosaltres, trucant al <a href="tel:938032993" class="phone-mail-link">93 803 29 93</a> i t'atendrem.
        </p>

        <!-- START - Contact Form -->
        <form id="contact-form" name="contact-form" method="POST" data-name="Contact Form">

            <div class="row">

                <!-- Full name -->
                <div class="col-xs-12 col-sm-6 col-lg-6">
                    <div class="form-group">
                        <input type="text" id="name" class="form form-control" placeholder="nom" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Name'" name="name" data-name="Name" required>
                    </div>
                </div>

                <!-- E-mail -->
                <div class="col-xs-12 col-sm-6 col-lg-6">
                    <div class="form-group">
                        <input type="email" id="email" class="form form-control" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email'" name="email-address" data-name="Email Address" required>
                    </div>
                </div>

                <!-- Subject -->
                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <div class="form-group">
                        <input type="text" id="subject" class="form form-control" placeholder="Tema" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Write the subject'" name="subject" data-name="Subject">
                    </div>
                </div>

                <!-- Message -->
                <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                    <div class="form-group">
                        <textarea id="text-area" class="form textarea form-control" placeholder="Escriu el teu missatge...." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your message here... 20 characters Min.'" name="message" data-name="Text Area" required></textarea>
                    </div>
                </div>

            </div>

            <!-- Button submit -->
            <button type="submit" id="valid-form" class="btn btn-color">Enviar el meu Missatge ></button>

        </form>
        <!-- END - Contact Form -->

        <!-- START - Answer for the contact form is displayed in the next div, do not remove it. -->       
        <div id="block-answer">

            <div id="answer"></div>

        </div>
        <!-- END - Answer... -->

        <h4>08700. Nits de Pells i Punt.</h4>

        <p>Vols saber <strong>més de nosaltres?</strong> <br>
            Si tens més suggeriments, preguntes o comentaris, aquí hi ha les millors formes de connectar amb nosaltres!
        </p>

        <br>

        <p>
            <i class="icon ion-ios-telephone"></i> <strong>Teléfon :</strong> <a href="tel:938032993" class="phone-mail-link">93 803 29 93</a><br>
            <i class="icon ion-ios-email"></i> <strong>Email :</strong> <a href="mailto:angels@fagepi.net" class="phone-mail-link">angels@fagepi.net</a> <br>
            <i class="icon ion-ios-location"></i> <strong>Adreça :</strong> <a href="https://goo.gl/maps/aAMjbLhempG2" class="phone-mail-link">Mestre Muntaner, 86. 08700 Igualada</a>
        </p>

    </div>
                

</section>
<!-- END - More Informations part -->

<!-- Cross to close the More Informations part -->
<button id="close-more-info" class="close-right-part hide-close"><i class="icon ion-android-close"></i></button>

<?= $this->load->view('includes/template/_solicitar') ?>
<?= $this->load->view('includes/template/_validacion') ?>

<!-- START - Root element of PhotoSwipe, the gallery. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
        It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Tancar (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Compartir"></button>

                <button class="pswp__button pswp__button--fs" title="Pantalla Complerta"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
<!-- END - Root element of PhotoSwipe, the gallery. Must have class pswp. -->

<div id="barracookies">
Fem servir galetes pròpies i de tercers que entre altres coses recullen dades sobre els seus hàbits de navegació per mostrar-li publicitat personalitzada i realitzar anàlisis d'ús del nostre lloc.
<br/>
Si continua navegant considerem que accepta el seu ús. <a href="javascript:void(0);" onclick="var expiration = new Date(); expiration.setTime(expiration.getTime() + (60000*60*24*365)); setCookie('avisocookies','1',expiration,'/');document.getElementById('barracookies').style.display='none';"><b>ACCEPTAR</b></a> <a href="http://www.google.com/intl/es-419/policies/technologies/types/" target="_blank" >
</div>
<!-- Estilo barra CSS -->
        <style>
            #barracookies {
                display: none;
                z-index: 99999;
                position:fixed;
                left:0px;
                right:0px;
                bottom:0px;
                width:100%;
                min-height:40px;
                padding:10px;
                background: #333333;
                color:white;
                line-height:20px;
                font-family:open sans, sans;
                font-size:12px;text-align:center;box-sizing:border-box;} #barracookies a:nth-child(2) {padding:3px;background:#00c8aa;border-radius:2px;text-decoration:none;} #barracookies a {color: #fff;text-decoration: none;}</style>
        <!-- Gestión de cookies-->
        <script type='text/javascript'>function setCookie(name,value,expires,path,domain,secure){document.cookie=name+"="+escape(value)+((expires==null)?"":"; expires="+expires.toGMTString())+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+((secure==null)?"":"; secure")}function getCookie(name){var cname=name+"=";var dc=document.cookie;if(dc.length>0){begin=dc.indexOf(cname);if(begin!=-1){begin+=cname.length;end=dc.indexOf(";",begin);if(end==-1)end=dc.length;return unescape(dc.substring(begin,end))}}return null}function delCookie(name,path,domain){if(getCookie(name)){document.cookie=name+"="+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+"; expires=Thu, 01-Jan-70 00:00:01 GMT"}}</script>
        <!-- Gestión barra aviso cookies -->
        <script type='text/javascript'>
        var comprobar = getCookie("avisocookies");
        if (comprobar != null) {}
        else {
        var expiration = new Date();
        expiration.setTime(expiration.getTime() + (60000*60*24*365));
        setCookie("avisocookies","1",expiration);
        document.getElementById("barracookies").style.display="block"; }
        </script>