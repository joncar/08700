<div class="page-header">
        <h1>
                <?= empty($title)?'Escritorio':$title ?>
                <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                </small>
        </h1>
</div><!-- /.page-header -->

<div class="row">
        <div class="col-xs-12">            
               <?= empty($crud)?'':$this->load->view('cruds/'.$crud) ?>                
        </div><!-- /.col -->
</div><!-- /.row -->
