<!DOCTYPE html>
<html lang="en-us" class="no-js">
    <head>
        <meta charset="utf-8">
        <title><?= empty($title)?'08700':$title ?></title>
        <meta name="description" content="The description should optimally be between 150-160 characters.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Madeon08">
        <link rel="shortcut icon" href="<?= base_url() ?>img/favicon.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>img/favicon-retina-ipad.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>img/favicon-retina-iphone.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>img/favicon-standard-ipad.png">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>img/favicon-standard-iphone.png">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/style.css" />
        <script src="<?= base_url() ?>js/template/modernizr.custom.js"></script>        
    </head>
    <body>
        <?= $this->load->view($view); ?>
        <script src="<?= base_url() ?>js/template/jquery.min.js"></script>
        <script src="<?= base_url() ?>js/template/jquery.easings.min.js"></script>
        <script src="<?= base_url() ?>js/template/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>js/template/notifyMe.js"></script>
        <script src="<?= base_url() ?>js/template/contact-me.js"></script>
        <script src="<?= base_url() ?>js/template/jquery.mousewheel.js"></script>
        <script src="<?= base_url() ?>js/template/jquery.mCustomScrollbar.js"></script>
        <script src="<?= base_url() ?>js/template/classie.js"></script>
        <script src="<?= base_url() ?>js/template/dialogFx.js"></script>
        <script src="<?= base_url() ?>js/template/photoswipe.js"></script> 
        <script src="<?= base_url() ?>js/template/photoswipe-ui-default.js"></script>
        <script src="<?= base_url() ?>js/template/jquery.countdown.js"></script>
        <script>
            console.log("asd");
            $("#getting-started") .countdown("2017/2/23 20:30:00", function (event) {
                $(this).html(
                        event.strftime('<strong>%D Dies</strong> %Hh %Mm %Ss')
                );
            });
        </script>
        <script src="<?= base_url() ?>js/template/bubble.js"></script>
        <script src="<?= base_url() ?>js/template/main.js"></script>
        <script src="<?= base_url() ?>js/template/selectFx.js"></script>
        <script src="<?= base_url() ?>js/template/kenburns.js"></script>
        <script>
                (function() {
                        [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
                                new SelectFx(el);
                        } );
                })();
        </script>
        <script type="text/javascript">
        $(function(){
            $('#canvasbg').kenburns({
                    images:['<?= base_url() ?>/img/main.jpg',
                            '<?= base_url() ?>/img/main2.jpg',
                            '<?= base_url() ?>/img/main3.jpg',
                            '<?= base_url() ?>/img/main4.jpg',
                            
                            
                    ],
                    frames_per_second: 20,
                    display_time: 30000,
                    fade_time: 1000,
                    zoom: 0,
                    background_color:'transparent',
                    post_render_callback:function($canvas, context) {
                            // Called after the effect is rendered
                            // Draw anything you like on to of the canvas

                            context.save();
                            context.fillStyle = '#000';
                            context.font = 'bold 20px sans-serif';
                            var width = $canvas.width();
                            var height = $canvas.height();												
                            var text = "";
                            var metric = context.measureText(text);

                            context.fillStyle = '#fff';

                            context.shadowOffsetX = 3;
                            context.shadowOffsetY = 3;
                            context.shadowBlur = 4;
                            context.shadowColor = 'rgba(0, 0, 0, 0.8)';

                            context.fillText(text, width - metric.width - 8, height - 8);						

                            context.restore();						
                    }
            });				
        });
        function IE(v) {
            return RegExp('msie' + (!isNaN(v)?('\\s'+v):''), 'i').test(navigator.userAgent);
        }
        //IF IE
        if(IE()){
            alert('Aquesta web no es soportada per el teu navegador. Actualitza Explorer a una versió més nova.');
        }
    </script>
    </body>   
    </html>